#ifdef GL_ES
	precision mediump float;
	precision lowp int;
#endif

varying vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_pal;

uniform int u_shadow;
uniform float u_transparent;

void main()
{  
	float alpha = 1.0;
	float index = texture2D(u_texture, v_texCoords).r;
	if(index == 0.0) 
		discard;

	if(u_transparent > 0.0)
		alpha = 10.0 * texture2D(u_texture, v_texCoords).r;


	/*	
		vec2 texelpos = v_texCoords * u_texsize;
		vec2 weight = mod(v_texCoords, 0.0);
		texelpos -= weight;
		float index_lb = texture2D(u_texture, texelpos / u_texsize).r;
		float index_rb = texture2D(u_texture, (texelpos + vec2(1,0)) / u_texsize).r;
		float index_lt = texture2D(u_texture, (texelpos + vec2(0,1)) / u_texsize).r;
		float index_rt = texture2D(u_texture, (texelpos + vec2(1,1)) / u_texsize).r;
		vec3 color_lb = texture2D(u_pal, vec2(index_lb, 0.0)).rgb;
		vec3 color_rb = texture2D(u_pal, vec2(index_rb, 0.0)).rgb;
		vec3 color_lt = texture2D(u_pal, vec2(index_lt, 0.0)).rgb;
		vec3 color_rt = texture2D(u_pal, vec2(index_rt, 0.0)).rgb;
		vec3 color_b = mix(color_lb, color_rb, weight.x);
		vec3 color_t = mix(color_lt, color_rt, weight.x);
		vec3 color = mix(color_b, color_t, weight.y);
		
		
		
		vec2 tex_size = u_tex_size;
		vec2 texelIndex = v_texCoords * tex_size.x;
		vec2 weight  = fract(texelIndex);
		texelIndex -= weight;
		texelIndex *= vec2(1.0/tex_size.s,1.0/tex_size.t);
		float index_0 = texture2D(u_texture, texelIndex).r;
		float index_1 = texture2D(u_texture, texelIndex + vec2(0.0, (1.0/tex_size.t))).r;
		float index_2 = texture2D(u_texture, texelIndex + vec2(1.0/tex_size.s,1.0/tex_size.t)).r;
		float index_3 = texture2D(u_texture, texelIndex + vec2((1.0/tex_size.s), 0.0)).r;
		vec3 color_0 = texture2D(u_pal, vec2(index_0, 0.0)).rgb;
		vec3 color_1 = texture2D(u_pal, vec2(index_1, 0.0)).rgb;
		vec3 color_2 = texture2D(u_pal, vec2(index_2, 0.0)).rgb;
		vec3 color_3 = texture2D(u_pal, vec2(index_3, 0.0)).rgb;
		vec3 c0 = (1.0-weight.s) * color_0 + weight.s * color_3;
		vec3 c1 = (1.0-weight.s) * color_1 + weight.s * color_2;
		gl_FragColor = vec4((1.0-weight.t) * c0 + weight.t * c1, 1.0);
	*/
	
	if(u_shadow != 1) {
		vec3 color = texture2D(u_pal, vec2(index, 0.0)).rgb;
		gl_FragColor = v_color * vec4(color, alpha);
	} else { 
		gl_FragColor = vec4(0, 0, 0, 0.5);
	}
}