#ifdef GL_ES
	precision lowp int;
	precision mediump float;
#endif

varying vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform int u_alpha;

void main()
{
	if (u_alpha == 1 && texture2D(u_texture, v_texCoords).a < 0.1)
		discard;

	gl_FragColor = v_color * texture2D(u_texture, v_texCoords);
}