#ifdef GL_ES
	precision lowp int;
	precision mediump float;
#endif

varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_pal;


void main()
{
	float alpha = 1.0;
	float index = texture2D(u_texture, v_texCoords).r;

	if(index == 0.0) 
		discard;	
	
	vec3 color = texture2D(u_pal, vec2(index, 0.5)).rgb;
	gl_FragColor = vec4(color, alpha);
}