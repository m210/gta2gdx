#ifdef GL_ES
	precision lowp int;
	precision highp float;
#endif

attribute vec3 a_position;
attribute vec2 a_texCoord0;
attribute vec4 a_color;

uniform int u_shadow;
uniform mat4 u_projTrans;
uniform mat4 u_worldTrans;

varying vec2 v_texCoords;
varying vec4 v_color;

void main() {
    v_color = a_color;
    v_texCoords = a_texCoord0;
    vec4 pos = u_worldTrans * vec4(a_position, 1.0);
    if(u_shadow == 1) {
    	float offset = u_projTrans[3][3] / 128.0;
    	pos.x += offset;
    	pos.y -= 0.05;
    	pos.z += offset;
    }
    gl_Position = u_projTrans * pos;
}