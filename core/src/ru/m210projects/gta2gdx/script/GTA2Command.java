//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.script;


public class GTA2Command {

	public int ptr;
	public int pointerIndex;
	public int type;
	public int nextPointerIndex;
	public int flags;

	public void setCommand(int ptr, int pointerIndex, int type, int nextPointerIndex, int flags) {
		this.ptr = ptr;
		this.pointerIndex = pointerIndex;
		this.type = type;
		this.nextPointerIndex = nextPointerIndex;
	}

	public static String[] cmdName = {
		"CMD0",
		"CMD1",
		"CMD2",
		"CMD3",
		"CMD4",
		"PLAYER_PED",
		"CHAR_DEC",
		"CHAR_DECSET_2D",
		"CHAR_DECSET_3D",
		"CAR_DEC",
		"CAR_DECSET_2D",
		"CAR_DECSET_3D",
		"CAR_DECSET_2D_STR",
		"CAR_DECSET_3D_STR",
		"OBJ_DEC",
		"OBJ_DECSET_2D",
		"OBJ_DECSET_3D",
		"OBJ_DECEST_2D_INT",
		"OBJ_DECSET_3D_INT",
		"OBJ_DECSET_2D_STR",
		"OBJ_DECSET_3D_STR",
		"COUNTER",
		"COUNTER_SET",
		"ARROW_DEC",
		"THREAD_ID",
		"CONVEYOR_DEC",
		"CONVEYOR_DECSET1",
		"CONVEYOR_DECSET2",
		"GENERATOR_DEC",
		"GENERATOR_DECSET1",
		"GENERATOR_DECSET2",
		"GENERATOR_DECSET3",
		"GENERATOR_DECSET4",
		"DESTRUCTOR_DEC",
		"DESTRUCTOR_DECSET1",
		"DESTRUCTOR_DECSET2",
		"CRANE_DEC",
		"CRANE_BASIC_DEC",
		"CRANE_TARGET_DEC",
		"CRANE2TARGET_DEC",
		"CRUSHER_BASIC",
		"CREATE_CHAR_2D",
		"CREATE_CHAR_3D",
		"CREATE_CAR_2D",
		"CREATE_CAR_3D",
		"CREATE_CAR_2D_STR",
		"CREATE_CAR_3D_STR",
		"CREATE_OBJ_2D",
		"CREATE_OBJ_3D",
		"CREATE_OBJ_3D_INT",
		"CREATE_OBJ_2D_INT",
		"CREATE_OBJ_3D_STR",
		"CREATE_OBJ_2D_STR",
		"CREATE_CONVEYOR_2D",
		"CREATE_CONVEYOR_3D",
		"CREATE_GENERATOR_2D",
		"CREATE_GENERATOR_3D",
		"CREATE_DESTRUCTOR_2D",
		"CREATE_DESTRUCTOR_3D",
		"LEVELSTART",
		"LEVELEND",
		"CREATE_THREAD",
		"STOR_THREAD",
		"START_EXEC",
		"STOR_EXEC",
		"FOR_LOOP",
		"DO_WHILE",
		"FUNCTION",
		"RETURN",
		"WHILE",
		"WHILE_EXEC",
		"NOT",
		"AND",
		"OR",
		"IF",
		"THEN",
		"ELSE",
		"GOTO",
		"GOSUB",
		"S_PLUS_I",
		"I_PLUS_S",
		"S_PLUS_S",
		"S_MINUS_I",
		"I_MINUS_S",
		"S_MINS_S",
		"SET",
		"S_LESS_I",
		"S_LESS_S",
		"S_LEQUAL_I",
		"S_LEQUAL_S",
		"S_GREATER_I",
		"S_GREATER_S",
		"S_GEQUAL_I",
		"S_GEQUAL_S",
		"S_EQUAL_I",
		"S_EQUAL_S",
		"INCREMENT",
		"DECREMENT",
		"IF_JUM",
		"FORWARD_DECLARE",
		"MAKE_CAR_DUMMY",
		"MAP_ZONE1",
		"MAP_ZONE_SET",
		"SET_CAR_DENSITY",
		"SET_GOOD_CAR",
		"SET_BAD_CAR",
		"SET_POLICE_CAR",
		"SET_PED_DENSITY",
		"SET_MUGGER",
		"SET_CARTHIEF",
		"SET_ELVIS",
		"SET_GANG",
		"SET_POLICE_PED",
		"POINT_ARROW_AT",
		"POINT_ARROW_3D",
		"ARROW_COLOUR",
		"REMOVE_ARROW",
		"DISPLAY_MESSAGE",
		"DISPLAY_BRIEF",
		"DISPLAY_TIMER",
		"CLEAR_TIMES",
		"IS_CHAR_IN_CAR",
		"IS_CHAR_IN_MODEL",
		"IS_CHAR_IN_ANY_CAR",
		"IS_CHAR_STOPPED",
		"IS_CHAR_STUNNED",
		"CHECK_HEALTH",
		"HAS_CHAR_DIED",
		"STORE_CAR_INFO",
		"CHECK_CAR_DAMAGE",
		"CHECK_CAR_DRIVER",
		"SET_CHAR_OBJ1",
		"SET_CHAR_OBJ2",
		"SET_CHAR_OBJ3",
		"IS_CHAR_OBJ_PASS",
		"IS_CHAR_OBJ_FAIL",
		"SEND_CHAR_FOOT",
		"SEND_CHAR_CAR",
		"GIVE_WEAPON_1",
		"IS_CAR_IN_BLOCK",
		"DELETE_ITEM",
		"ADD_SCORE1",
		"EXPLODE",
		"EXPLODE_BUILDING",
		"EXPLODE_ITEM",
		"LOCATE_CHAR_ANY",
		"LOCATE_CHAR_ONFOOT",
		"LOCATE_CHAR_BY_CAR",
		"STOP_LOCATE_CHAR_ANY",
		"STOP_LOCATE_CHAR_FOOT",
		"STOP_LOCATE_CHAR_CAR",
		"SET_THREAT_SEARCH",
		"SET_THREAT_REACT",
		"ADD_GROUP",
		"IS_CAR_WRECKED",
		"CHANGE_CAR_REMAP",
		"CHANGE_CHAR_REMAP",
		"CHECK_CAR_MODEL",
		"CHECK_CAR_REMAP",
		"CHECK_CAR_BOTH",
		"IS_ITEM_ONSCREEN",
		"DELAY_HERE",
		"DELAY",
		"CLEAR_WANTED_LEVEL",
		"ALT_WANTED_LEVEL",
		"IS_CHAR_FIRE_ONSCREEN",
		"DRIVER_OUT_CAR",
		"CHAR_TO_DRIVE_CAR",
		"ANSWER_PHONE",
		"SEND_CAR_TO_BLOCK",
		"GIVE_DRIVER_BRAKE",
		"CHAR_TO_BACKDOOR",
		"PLAY_SOUND",
		"SET_NO_COLLIDE",
		"CLEAR_NO_COLLIDE",
		"CAR_DRIVE_AWAY",
		"IS_CHAR_FIRING_AREA",
		"OPEN_DOOR",
		"CLOSE_DOOR",
		"SET_DOOR_AUTO",
		"SET_DOOR_MANUAL",
		"ROAF_ON_OFF",
		"ADD_NEW_BLOCK",
		"REMOVE_BLOCK",
		"LOWER_LEVEL",
		"CHANGE_BLOCK_SIDE",
		"CHANGE_BLOCK_LID",
		"CHANGE_BLOCK_TYPE",
		"GET_CAR_FROM_CRANE",
		"CAR_WRECK_IN_LOCATION",
		"CAR_SUNK",
		"CAR_IN_AIR",
		"CHECK_NUM_LIVES",
		"GET_NUM_LIVES",
		"CHECK_SCORE",
		"GET_SCORE",
		"CHECK_MULT",
		"GET_MULT",
		"CHECK_RESPECT_GREATER",
		"CHECK_RESPECT_LESS",
		"CAR_DAMAGE_POS",
		"GET_PASSENGER_NUM",
		"CHAR_IN_AIR",
		"CHAR_SUNK",
		"LAST_WEAPON_HIT",
		"ADD_PATROL_POINT",
		"GET_CAR_SPEED",
		"GET_CHAR_CAR_SPEED",
		"CHECK_CAR_SPEED",
		"GET_MAX_SPEED",
		"THREAD_DECLARE1",
		"THREAD_DECLARE2",
		"THREAD_DECLARE3",
		"THREAD_DECLARE4",
		"THREAD_DECLARE5",
		"ENABLE_THREAD",
		"DISABLR_THREAD",
		"LIGHT_DEC",
		"LIGHT_DECSET1",
		"CREATE_LIGHT1",
		"CHANGE_INTENSITY",
		"CHANGE_COLOUR",
		"CHANGE_RADIUS",
		//"SET_CHAR_MOM_FAT",
		"SET_GANG_INFO1",
		"SET_GANG_RESPECT",
		"SET_CHAR_RESPECT",
		"SET_AMBIENT",
		"CHECK_PHONE",
		"CHECK_PHONETIMER",
		"STOP_PHONE_RING",
		"SET_DOOR_INFO",
		"CHECK_RESPECT_IS",
		"IS_CHAR_IN_GANG",
		"TIMER_DECLARE",
		"CHECK_NUM_ALIVE",
		"ADD_CHAR_TO_GROUP",
		"REMOVE_CHAR",
		"SET_MIN_ALIVE",
		"SET_CHAR_SHOOT",
		"SET_CHAR_BRAVERY",
		"DECLARE_MISSION",
		"HAS_CAR_WEAPON",
		"IS_CHAR_IN_ZONE",
		"IS_CHAR_HORN",
		"CHECK_MAX_PASS",
		"SET_PHONE_DEAD",
		"IS_TRAILER_ATT",
		"IS_CAR_ON_TRAIL",
		"ENABLE_CRANE",
		"DISABLE_CRANE",
		"CAR_GOT_DRIVER",
		"SPOTTED_PLAYER",
		"GET_LAST_PUNCHED",
		"KILL_ALL_PASSENG",
		"IS_GROUP_IN_CAR",
		"PUNCHED_SOMEONE",
		"REMOVE_WEAPON",
		"DO_NOWT",
		"ADD_CHAR_TO_GANG",
		"MAKE_LEADER",
		"PARK",
		"PARK_FINISHED",
		"CHANGE_RESPECT",
		"PHONE_TEMPLATE",
		"BEEN_PUNCHED_BY",
		"UPDATE_DOOR",
		"GIVE_WEAPON2",
		"IS_CAR_CRUSHED",
		"SWITCH_GENERATOR1",
		"SWITCH_GENERATOR2",
		"CAR_IN_AREA",
		"REMOTE_CONTROL",
		"MISSIONSTART",
		"MISSIONEND",
		"LAUNCH_MISSION",
		"COUNTER_SAVE",
		"COUNTER_SET_SAVE",
		"SAVE_GAME",
		"CHANGE_CAR_LOCK",
		"DISPLAY_BRIEF_NOW",
		"PARK_NO_RESPAWN",
		"ADD_LIVES",
		"ADD_MULTIPLIER",
		"SET_DIR_OF_TVVAN",
		"POINT_ONSCREEN",
		"SET_STATION",
		"SET_EMPTY_STATION",
		"RADIOSTATION_DEC",
		"START_BONUS1",
		"START_BONUS2",
		"START_BONUS3",
		"START_BONUS4",
		"CHECK_BONUS1",
		"CHECK_BONUS2",
		"CHECK_BONUS3",
		"SET_STATION_4",
		"SET_STATOIN_3",
		"SET_STATION_2",
		"SET_STATION_1",
		"SETUP_MODEL_CHECK",
		"MODEL_CHECK",
		"LIGHT_DECSET2",
		"CREATE_LIGHT2",
		"SET_CAR_GRAPHIC",
		"DECLARE_POLICE",
		"CARBOMB_ACTIVE",
		"CHAR_DRIVE_AGGR",
		"CHAR_DRIVE_SPEED",
		"S_IS_S_MINUS_S",
		"S_IS_S_PLUS_S",
		"GIVE_CAR_ALARM",
		"CAR_BULLETPROOF",
		"CAR_ROCKETPROOF",
		"CAR_FLAMEPROOF",
		"IS_ALARM_RINGING",
		"SET_CHAR_OBJ_FOLLOW",
		"PUT_CAR_ON_TRAILER",
		"CLEAR_BRIEFS",
		"CHECK_HEADS",
		"FINISH_LEVEL",
		"CHECK_WEAPONHIT",
		"DISPLAY_BRIEF_SOON",
		"SET_GANG_INFO2",
		"MAP_ZONE2",
		"IS_CHAR_ON_FIRE",
		"BRIEF_ONSCREEN",
		"SOUND",
		"SOUND_DECSET",
		"CREATE_SOUND",
		"DO_EASY_PHONE",
		"CHAR_INTO_CAR",
		"DEL_GROUP_IN_CAR",
		"S_IS_S_MINUS_I",
		"S_IS_S_PLUS_I",
		"S_IS_S_DIV_I",
		"S_IS_S_MULT_I",
		"S_IS_S_MOD_I",
		"S_IS_S_DIV_S",
		"S_IS_S_MULT_S",
		"S_IS_S_MOD_S",
		"SET_COUNTER_INT",
		"SET_COUNTER_VAR",
		"ADD_SCORE2",
		"FINISH_SCORE",
		"TOTAL_MISSIONS",
		"TOTAL_SECRETS",
		"PASSED_FLAG",
		"CMD1_PASSED_FLAG",
		"CMD2_PASSED_FLAG",
		"CMD3_PASSED_FLAG",
		"SUPPRESS_MODEL",
		"SWITCH_GENERATOR3",
		"SWITCH_GENERATOR4",
		"DECLARE_CARLIST",
		"DECIDE_POWERUP",
		"CHAR_ARRESTED",
		"ONSCREEN_ACCURACY",
		"WARP_CHAR",
		"WEAP_HIT_CAR",
		"SET_GROUP_TYPE",
		"CHAR_DO_NOTHING",
		"EMERG_LIGHTS",
		"CHAR_INVINCIBLE",
		"PED_GRAPHIC",
		"MAKE_MUGGERS",
		"ANY_WEAPON_HIT_CAR",
		"LOC_SECOND_CHAR",
		"CHECK_OBJ_MODEL",
		"STOP_CAR_DRIVE",
		"IS_BUS_FULL",
		"NO_CHARS_OFF_BUS",
		"KILL_CHAR",
		"SET_GANGCARRATIO",
		"SET_SHADING_LEV",
		"SET_CAR_JAMMED",
		"DOOR_DECLARE_S1",
		"DOOR_DECLARE_S2",
		"DOOR_DECLARE_S3",
		"DOOR_DECLARE_D1",
		"DOOR_DECLARE_D2",
		"DOOR_DECLARE_D3",
		"LOC_SEC_CHAR_CAR",
		"LOC_SEC_CHAR_ANY",
		"SET_RUN_SPEED",
		"SET_STAY_IN_CAR",
		"SET_USE_CAR_WEAPON",
		"GANG_1_MISSION_TOTAL",
		"GANG_2_MISSION_TOTAL",
		"GANG_3_MISSION_TOTAL",
		"SECRETS_PASSED",
		"SECRETS_FAILED",
		"FINISH_MISSION",
		"ADDSCORE_NO_MULT",
		"CHANGE_GANG_RESP",
		"CREATE_GANG_CAR1",
		"CREATE_GANG_CAR2",
		"CREATE_GANG_CAR3",
		"CREATE_GANG_CAR4",
		"EXPLODE_LARGE1",
		"EXPLODE_LARGE2",
		"SET_ENTER_STATUS",
		"BONUS_DECLARE",
		"STORE_BONUS",
		"EXPLODE_SMALL1",
		"EXPLODE_SMALL2",
		"EXPLODE_NO_RING1",
		"EXPLODE_NO_RING2",
		"SET_ALL_CONTROLS",
		"SET_FAV_CAR",
		"GROUP_IN_AREA",
		"SET_CHAR_OCCUPATION",
		"SET_KF_WEAPON",
		"CLEAR_KF_WEAPON",
		"ONSCREEN_COUNTER_DEC",
		"ADD_ONSCREEN_COUNTER",
		"CLEAR_COUNTER",
		"CLEAR_CLOCK_ONLY",
		"EMERG_LIGHTS_ON",
		"CHANGE_POLICE",
		"DESTROY_GROUP",
		"CHECK_CURRENT_WEAPON",
		"ALTER_WANTED_LEVEL",
		"START_BASIC_KF",
		"DO_BASIC_KF",
		"SET_BONUS_RATING",
		"PARKED_CAR_DECSET_2D",
		"PARKED_CAR_DECSET_3D",
		"PARCKED_CAR_DECSET_2D_STR",
		"PARKED_CAR_DECSET_3D_STR",
		"DEC_GANG_1_FLAG",
		"DEC_GANG_2_FLAG",
		"DEC_GANG_3_FLAG",
		"DEATH_ARR_STAET",
		"ADD_TIME",
		"CHAR_AREA_ANY_MEANS",
		"DEC_DEATH_BASE_1",
		"DEC_DEATH_BASE_2",
		"DEC_DEATH_BASE_3",
		"DO_SAVE_GAME",
		"DO_CRANE_POWERUP",
		"LEVEL_END_ARROW1",
		"LEVEL_END_ARROW2",
		"SET_MODEL_WANTED",
		"CHECK_DEATH_ARR",
		"FORCE_CLEANUP",
		"SAVE_RESPECT",
		"RESTORE_RESPECT"
		}; //446
}
