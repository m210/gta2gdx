//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.script;

import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.objects.Car;
import ru.m210projects.gta2gdx.objects.Phone;
import ru.m210projects.gta2gdx.objects.Pickup;
import ru.m210projects.gta2gdx.objects.Player;
import ru.m210projects.gta2gdx.objects.Sprite;
import ru.m210projects.gta2gdx.tools.IOStream;
import ru.m210projects.gta2gdx.tools.LittleEndian;

public class ScrReader {
	private IOStream io;
	private boolean debug = false;
	private int handle;

	private static byte[] script;
	private static int[] pointers;
	private static String[] strings = new String[5120];
	private GTA2Command cmd;
	private World world;

	private int[] sprite_point;

	public ScrReader(String path, World world) {
		this.world = world;
		io = new IOStream();
		handle = init(path);
		close();
	}

	public int init(String path) {
		int fis = io.open(path);

		pointers = new int[6000];
		byte[] buf = new byte[2 * pointers.length];
		io.read(fis, buf, buf.length);

		int maxpointer = 0;
		for(int i = 0; i < pointers.length; i++) {
			pointers[i] = LittleEndian.getUShort(buf, i * 2);
			if(pointers[i] > maxpointer)
				maxpointer = pointers[i];
		}

		script = new byte[65536];
		io.read(fis, script, script.length);

		buf = new byte[2];
		io.read(fis, buf, buf.length);
		int size = LittleEndian.getUShort(buf);

		buf = new byte[size];
		io.read(fis, buf, buf.length);

		int offset = 0;
		while (offset < size) {
			int id = LittleEndian.getInt(buf, offset + 0);
            int type = LittleEndian.getInt(buf, offset + 4);
            short length = LittleEndian.getUByte(buf, offset + 8);
            offset += 9;
            strings[id] = readNullString(buf, offset, length);
            offset += length;

            log(type + " "+ id +" "+strings[id]);
		}
		int maxIndex = LittleEndian.getUShort(script, maxpointer);
		sprite_point = new int[maxIndex+1];

		cmd = new GTA2Command();
		for(int ptr : pointers) {
			if (ptr == 0) {
                continue;
            }

			int pointerIndex = LittleEndian.getUShort(script, ptr);
			int type = LittleEndian.getUShort(script, ptr+2);
			int nextPointerIndex = LittleEndian.getUShort(script, ptr+4);
			int flags = LittleEndian.getUShort(script, ptr+6);
			String name = GTA2Command.cmdName[type];
			cmd.setCommand(ptr, pointerIndex, type, nextPointerIndex, flags);
			//System.out.println(name);
			switch(name) {
				case "PLAYER_PED":
					readPlayer();
				break;
				case "OBJ_DEC":
	            case "CREATE_OBJ_3D_INT":
	            case "OBJ_DECSET_3D":
	            case "OBJ_DECSET_3D_STR":
	            case "OBJ_DECSET_3D_INT":
	            case "CREATE_OBJ_2D_STR":
	            case "OBJ_DECSET_2D":
	            case "OBJ_DECSET_2D_STR":
	            case "OBJ_DECSET_2D_INT":
					readObjects();
				break;
	            case "PARKED_CAR_DECSET_2D":
                case "PARKED_CAR_DECSET_2D_STR":
                case "PARKED_CAR_DECSET_3D":
                case "PARKED_CAR_DECSET_3D_STR":
                case "CAR_DECSET_2D":
                case "CAR_DECSET_2D_STR":
                case "CAR_DECSET_3D":
                case "CAR_DECSET_3D_STR":
                case "CAR_DEC":
                case "CREATE_CAR_3D":
                case "CREATE_CAR_3D_STR":
                case "CREATE_CAR_2D":
                case "CREATE_CAR_2D_STR":
                case "CREATE_GANG_CAR1":
                case "CREATE_GANG_CAR2":
                case "CREATE_GANG_CAR3":
                case "CREATE_GANG_CAR4":
                    readCars();
                break;
                case "PUT_CAR_ON_TRAILER":
                	putCarOnTrailer();
                break;
                case "GENERATOR_DECSET2": //FIXME:
                case "GENERATOR_DECSET4":
                	readGenerators();
                break;

			}

			sprite_point[cmd.pointerIndex] = world.obj.size() - 1;
			log("[GTA2] " + pointerIndex + " " + GTA2Command.cmdName[type] + " " + nextPointerIndex + " " + flags);
		}
		return fis;
	}

    private void putCarOnTrailer() {
        int offset = cmd.ptr+8;
        int car = LittleEndian.getUShort(script, offset);
		int trailerid = LittleEndian.getShort(script, offset+2);

		Car vehicle = (Car) world.obj.get(sprite_point[car]);
		Sprite trailer = world.obj.get(sprite_point[trailerid]);

		vehicle.pos.set(trailer.pos);
		vehicle.pos.y += 0.01;
        vehicle.ang = (trailer.ang + 180) % 360;
        vehicle.setTrailer(sprite_point[trailerid]);
    }

	public void readCars() {
		int offset = cmd.ptr+8;
		//int varName = LittleEndian.getUShort(script, offset);
		//int unknown = LittleEndian.getUShort(script, offset+2);
		float x = (LittleEndian.getInt(script, offset+4))/16384.0f;
		float y = LittleEndian.getInt(script, offset+8)/16384.0f;
		float z = LittleEndian.getInt(script, offset+12)/16384.0f;
		int rotation = (LittleEndian.getUShort(script, offset+16) - 90) % 360;
		short remap = LittleEndian.getShort(script, offset+18);
		int carModel = LittleEndian.getUShort(script, offset+20);
		int trailerModel = LittleEndian.getUShort(script, offset+22);// 0xFFFF if no trailer, 0xFFFE if MINI_CAR.

        if (trailerModel == 0xFFFE) {
        	//System.out.println("MINI_CAR");
        } else if (trailerModel != 0xFFFF) {
        	//System.out.println("TRAILER");
        }

		if(GTA2Command.cmdName[cmd.type].equals("PARKED_CAR_DECSET_2D") || GTA2Command.cmdName[cmd.type].equals("CAR_DECSET_2D"))
			z = getZ(x, y);

		x += World.PARTSIZE;
		y += World.PARTSIZE;

		Car car = new Car(x, y, z, rotation, carModel, remap, true);

		world.obj.add(car);
	}

	public void readPlayer() {
		int offset = cmd.ptr+8;
		int unk1 = LittleEndian.getUShort(script, offset);
		int unk2 = LittleEndian.getUShort(script, offset+2);
		float x = (LittleEndian.getInt(script, offset+4))/16384.0f;
		float y = LittleEndian.getInt(script, offset+8)/16384.0f;
		float z = LittleEndian.getInt(script, offset+12)/16384.0f;
		int rotation = (LittleEndian.getUShort(script, offset+16) + 270) % 360;
		short remap = LittleEndian.getShort(script, offset+18);
		int unk3 = LittleEndian.getUShort(script, offset+20);

		if(z == 255)
			z = getZ(x, y);
		x += World.PARTSIZE;
		y += World.PARTSIZE;

		world.player = new Player(x, y, z, rotation, 1, remap);
		world.obj.add(world.player);

		log("PLAYER_PED p1 " + x + " " + y + " " + z + " " + rotation + " " + remap + " " + unk1 + " " + unk2 + " " +unk3);
	}

	public void readGenerators() {
		int offset = cmd.ptr+8;

		//int varName = LittleEndian.getUShort(script, offset);
		//int unknown = LittleEndian.getUShort(script, offset+2);
		float x = (LittleEndian.getInt(script, offset+4))/16384.0f;
		float y = LittleEndian.getInt(script, offset+8)/16384.0f;
		float z = LittleEndian.getInt(script, offset+12)/16384.0f;
		int rotation = LittleEndian.getUShort(script, offset+16);
		int entityId = LittleEndian.getUShort(script, offset+18);

		if(z == 255)
			z = getZ(x, y);
		x += World.PARTSIZE;
		y += World.PARTSIZE;

		String entity = GTA2Entity.entity_id[entityId];
		if(entity.contains("MOVING_COLLECT_"))
			world.obj.add(new Pickup(entityId - 65, x, y, z, rotation));
		else if(entity.contains("COLLECT_"))
			world.obj.add(new Pickup(entityId - 200, x, y, z, rotation));
	}

	public void readObjects() {
		int offset = cmd.ptr+8;

		//int varName = LittleEndian.getUShort(script, offset);
		//int unknown = LittleEndian.getUShort(script, offset+2);
		float x = (LittleEndian.getInt(script, offset+4))/16384.0f;
		float y = LittleEndian.getInt(script, offset+8)/16384.0f;
		float z = LittleEndian.getInt(script, offset+12)/16384.0f;
		int entityId = LittleEndian.getUShort(script, offset+16);
		int rotation = LittleEndian.getUShort(script, offset+18);

		if(z == 255)
			z = getZ(x, y);
		x += World.PARTSIZE;
		y += World.PARTSIZE;

		String entity = GTA2Entity.entity_id[entityId];


		if(entity.contains("COLLECT_"))
			world.obj.add(new Pickup(entityId - 200, x, y, z, rotation));
		if(entity.contains("BONUS_TOKEN"))
			world.obj.add(new Pickup(43, x, y, z, rotation));
		if(entity.contains("KILL_FRENZY"))
			world.obj.add(new Pickup(41, x, y, z, rotation));
		if(entity.equals("PHONE"))
			world.obj.add(new Phone(x, y, z, rotation, 0));
		if(entity.equals("GREEN_PHONE"))
			world.obj.add(new Phone(x, y, z, rotation, 1));
		if(entity.equals("YELLOW_PHONE"))
			world.obj.add(new Phone(x, y, z, rotation, 3));
		if(entity.equals("RED_PHONE"))
			world.obj.add(new Phone(x, y, z, rotation, 2));
	}

	public void close() {
		sprite_point = null;
		script = null;
		pointers = null;
		strings = null;
		io.close(handle);
	}

	private String readNullString(byte[] buf, int offset, int length) {
        int nullIndex = length;
        for (int index = 0; index < length; index++) {
            if (buf[offset + index] == 0) {
                nullIndex = index;
                break;
            }
        }
        return new String(buf, offset, nullIndex);
    }

	public int getZ(float x, float y) {
		return World.getZ(x, y);
    }

	private void log(String txt) {
		if(debug)
			System.out.println(txt);
	}
}
