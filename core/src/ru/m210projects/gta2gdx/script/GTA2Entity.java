//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.script;

public class GTA2Entity {
	public static String[] entity_id = {
		"UNKNOWN000",
	    "BIN_LID",
	    "BOLLARD",
	    "CONE",
	    "BOXES",
	    "BLASTER",
	    "RUBBISH",
	    "BIN",
	    "ANIMATING_OIL",
	    "OIL",
	    "MINE",
	    "BUSH",
	    "CRATE",
	    "FOOTY",
	    "HARDBOX",
	    "NEWSDIS",
	    "OILDRUM",
	    "TYRE",
	    "HYDRANT_LID",
	    "HYDRANT",
	    "HYDRANT_UNLID",
	    "ROADBLOCK",
	    "BENCH",
	    "PACKAGE",
	    "UNKNOWN024",
	    "TOWER",
	    "UNKNOWN026",
	    "UNKNOWN027",
	    "UNKNOWN028",
	    "UNKNOWN029",
	    "UNKNOWN030",
	    "UNKNOWN031",
	    "UNKNOWN032",
	    "UNKNOWN033",
	    "UNKNOWN034",
	    "UNKNOWN035",
	    "UNKNOWN036",
	    "UNKNOWN037",
	    "UNKNOWN038",
	    "UNKNOWN039",
	    "UNKNOWN040",
	    "UNKNOWN041",
	    "EXPLODE_MEDIUM",
	    "UNKNOWN043",
	    "UNKNOWN044",
	    "UNKNOWN045",
	    "UNKNOWN046",
	    "UNKNOWN047",
	    "UNKNOWN048",
	    "UNKNOWN049",
	    "UNKNOWN050",
	    "UNKNOWN051",
	    "UNKNOWN052",
	    "UNKNOWN053",
	    "UNKNOWN054",
	    "UNKNOWN055",
	    "UNKNOWN056",
	    "UNKNOWN057",
	    "UNKNOWN058",
	    "UNKNOWN059",
	    "UNKNOWN060",
	    "UNKNOWN061",
	    "UNKNOWN062",
	    "UNKNOWN063",
	    "MOVING_COLLECT_00",
	    "MOVING_COLLECT_01",
	    "MOVING_COLLECT_02",
	    "MOVING_COLLECT_03",
	    "MOVING_COLLECT_04",
	    "MOVING_COLLECT_05",
	    "MOVING_COLLECT_06",
	    "MOVING_COLLECT_07",
	    "MOVING_COLLECT_08",
	    "MOVING_COLLECT_09",
	    "MOVING_COLLECT_10",
	    "MOVING_COLLECT_11",
	    "MOVING_COLLECT_12",
	    "MOVING_COLLECT_13",
	    "MOVING_COLLECT_14",
	    "MOVING_COLLECT_15",
	    "MOVING_COLLECT_16",
	    "MOVING_COLLECT_17",
	    "MOVING_COLLECT_18",
	    "MOVING_COLLECT_19",
	    "MOVING_COLLECT_20",
	    "MOVING_COLLECT_21",
	    "MOVING_COLLECT_22",
	    "MOVING_COLLECT_23",
	    "MOVING_COLLECT_24",
	    "MOVING_COLLECT_25",
	    "MOVING_COLLECT_26",
	    "MOVING_COLLECT_27",
	    "MOVING_COLLECT_28",
	    "MOVING_COLLECT_29",
	    "MOVING_COLLECT_30",
	    "MOVING_COLLECT_31",
	    "MOVING_COLLECT_32",
	    "MOVING_COLLECT_33",
	    "MOVING_COLLECT_34",
	    "MOVING_COLLECT_35",
	    "MOVING_COLLECT_36",
	    "MOVING_COLLECT_37",
	    "MOVING_COLLECT_38",
	    "MOVING_COLLECT_39",
	    "MOVING_COLLECT_40",
	    "MOVING_COLLECT_41",
	    "MOVING_COLLECT_42",
	    "MOVING_COLLECT_43",
	    "MOVING_COLLECT_44",
	    "SMALL_ARROW",
	    "UNKNOWN110",
	    "BLOOD_SPARK",
	    "UNKNOWN112",
	    "PARTICLE_SYSTEM",
	    "FIREJET",
	    "UNKNOWN115",
	    "SMALL_BROWN_SKID",
	    "SMALL_GREY_SKID",
	    "SMALL_RED_SKID",
	    "MEDIUM_BROWN_SKID",
	    "MEDIUM_GREY_SKID",
	    "MEDIUM_RED_SKID",
	    "CAR_CROSSING",
	    "CAR_STOP",
	    "BIG_WHITE_SKID",
	    "MEDIUM_WHITE_SKID",
	    "SMALL_WHITE_SKID",
	    "UNKNOWN127",
	    "ROCKET",
	    "BUS_STOP_MARKER",
	    "CAR_SHOP",
	    "BUSY_CAR_SHOP",
	    "CAR_BOMB",
	    "UNKNOWN133",
	    "UNKNOWN134",
	    "UNKNOWN135",
	    "UNKNOWN136",
	    "UNKNOWN137",
	    "MOLOTOV_MOVING",
	    "UNKNOWN139",
	    "UNKNOWN140",
	    "UNKNOWN141",
	    "UNKNOWN142",
	    "UNKNOWN143",
	    "HUGE_RED_SKID",
	    "HUGE_WHITE_SKID",
	    "HUGE_BROWN_SKID",
	    "HUGE_GREY_SKID",
	    "TANKTOP",
	    "ANTENNA",
	    "UNKNOWN150",
	    "ANIMATING_RUBBISH",
	    "DEAD_RUBBISH",
	    "UNKNOWN153",
	    "UNKNOWN154",
	    "MOVING_CONE",
	    "UNKNOWN156",
	    "MOVING_BIN",
	    "MOVING_BIN_LID",
	    "UNKNOWN159",
	    "UNKNOWN160",
	    "UNKNOWN161",
	    "UNKNOWN162",
	    "PHONE",
	    "PHONE_RINGING",
	    "UNKNOWN165",
	    "UNKNOWN166",
	    "UNKNOWN167",
	    "UNKNOWN168",
	    "UNKNOWN169",
	    "UNKNOWN170",
	    "UNKNOWN171",
	    "UNKNOWN172",
	    "UNKNOWN173",
	    "PHONE_DEAD",
	    "BRIEFCASE",
	    "RED_PHONE",
	    "RED_PHONE_RINGING",
	    "YELLOW_PHONE",
	    "YELLOW_PHONE_RINGING",
	    "GREEN_PHONE",
	    "GREEN_PHONE_RINGING",
	    "GRENADE",
	    "UNKNOWN183",
	    "UNKNOWN184",
	    "UNKNOWN185",
	    "UNKNOWN186",
	    "UNKNOWN187",
	    "UNKNOWN188",
	    "UNKNOWN189",
	    "UNKNOWN190",
	    "UNKNOWN191",
	    "SHOT",
	    "UNKNOWN193",
	    "FLAMING_BULLET",
	    "UNKNOWN195",
	    "UNKNOWN196",
	    "FIRE",
	    "WATER_BULLET",
	    "UNKNOWN199",
	    "COLLECT_00",
	    "COLLECT_01",
	    "COLLECT_02",
	    "COLLECT_03",
	    "COLLECT_04",
	    "COLLECT_05",
	    "COLLECT_06",
	    "COLLECT_07",
	    "COLLECT_08",
	    "COLLECT_09",
	    "COLLECT_10",
	    "COLLECT_11",
	    "COLLECT_12",
	    "COLLECT_13",
	    "COLLECT_14",
	    "COLLECT_15",
	    "COLLECT_16",
	    "COLLECT_17",
	    "COLLECT_18",
	    "COLLECT_19",
	    "COLLECT_20",
	    "COLLECT_21",
	    "COLLECT_22",
	    "COLLECT_23",
	    "COLLECT_24",
	    "COLLECT_25",
	    "COLLECT_26",
	    "COLLECT_27",
	    "COLLECT_28",
	    "COLLECT_29",
	    "COLLECT_30",
	    "COLLECT_31",
	    "COLLECT_32",
	    "COLLECT_33",
	    "COLLECT_34",
	    "COLLECT_35",
	    "COLLECT_36",
	    "COLLECT_37",
	    "COLLECT_38",
	    "COLLECT_39",
	    "COLLECT_40",
	    "COLLECT_41",
	    "COLLECT_42",
	    "COLLECT_43",
	    "COLLECT_44",
	    "UNKNOWN245",
	    "UNKNOWN256",
	    "BOMB",
	    "UNKNOWN248",
	    "BIG_BROWN_SKID",
	    "BIG_GREY_SKID",
	    "UNKNOWN251",
	    "UNKNOWN252",
	    "BIG_RED_SKID",
	    "BULLET",
	    "TRAFFIC_LIGHT",
	    "RED_FOOTPRINTS",
	    "BLOOD",
	    "CROSSING",
	    "SPARK",
	    "UNKNOWN260",
	    "UNKNOWN261",
	    "UNKNOWN262",
	    "UNKNOWN263",
	    "UNKNOWN264",
	    "PISTOL_BULLET",
	    "BONUS_TOKEN",
	    "UNKNOWN267",
	    "UNKNOWN268",
	    "UNKNOWN269",
	    "UNKNOWN270",
	    "UNKNOWN271",
	    "UNKNOWN272",
	    "UNKNOWN273",
	    "UNKNOWN274",
	    "UNKNOWN275",
	    "UNKNOWN276",
	    "TASER_BULLET",
	    "UNKNOWN277",
	    "SOUND_INSTANT",
	    "INVISIBLE_TARGET",
	    "POWERGEN",
	    "POWERGEN_DEAD",
	    "INVISIBLE_DESTRUCTIBLE",
	    "GENLITE",
	    "INVISIBLE_DEAD",
	    "KILL_FRENZY",
	    "UNKNOWN287",
	    "UNKNOWN288",
	    "UNKNOWN289",
	    "UNKNOWN290",
	    "UNKNOWN291",
	    "UNKNOWN292",
	    "UNKNOWN293",
	    "TUNNEL_BLOCKER",
	    "REMOTE"
	};
}
