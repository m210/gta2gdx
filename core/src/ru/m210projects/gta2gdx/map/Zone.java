//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map;

import com.badlogic.gdx.math.Vector2;

public class Zone {
	private int zone_type;
	private String name;
	private Vector2 pos;
	private Vector2 dimensions;

	public Zone(int zone_type, int x, int y, int w, int h, String name) {
		pos = new Vector2(x, y);
		dimensions = new Vector2(w, h);
		this.zone_type = zone_type;
		this.name = name;
	}

	@Override
	public String toString() {
		return ("name "+ name + ", pos[" + pos.x + "," + pos.y + "]," + " zone_type " + zone_type);
	}

	public Vector2 getPosition() {
		return pos;
	}

	public Vector2 getDimensions() {
		return dimensions;
	}

	public String getName() {
		return name;
	}

	public int getType() {
		return zone_type;
	}
}
