//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map;

import ru.m210projects.gta2gdx.tools.BitHelper;

public class BlockInfo {

	public static byte AIR = 0;
	public static byte ROAD = 1;
	public static byte PAVEMENT = 2;
	public static byte FIELD = 3;

	public short left, right, top, bottom, lid;
	public byte arrows;
	public int slope_type;
	public int ground_type;
	public boolean[] wall_type = new boolean[4];
	public boolean[] bullet_type = new boolean[4];

	public static final float SIZE = 1.0f;

	public BlockInfo(short lf, short rt, short tp, short bm, short ld, byte aw, byte st) {
		left = lf;
		right = rt;
		top = tp;
		bottom = bm;
		lid = ld;
		arrows = aw;
		slope_type = st;

		ground_type = 0;
		for(byte i = 2; i < 8; i++)
			ground_type = (byte) BitHelper.SetBit(slope_type, i, false);
		ground_type = ground_type & 0x03;


		slope_type = BitHelper.SetBit(slope_type,(byte) 0, false);
		slope_type = BitHelper.SetBit(slope_type,(byte) 1, false);
		slope_type = (slope_type >> 2) & 0x3F;


		wall_type[0] = BitHelper.CheckBit(top, 10);
		wall_type[1] = BitHelper.CheckBit(bottom, 10);
		wall_type[2] = BitHelper.CheckBit(left, 10);
		wall_type[3] = BitHelper.CheckBit(right, 10);

		bullet_type[0] = BitHelper.CheckBit(top, 11);
		bullet_type[1] = BitHelper.CheckBit(bottom, 11);
		bullet_type[2] = BitHelper.CheckBit(left, 11);
		bullet_type[3] = BitHelper.CheckBit(right, 11);
	}
}
