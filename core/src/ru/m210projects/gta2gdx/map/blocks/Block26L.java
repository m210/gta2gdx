//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map.blocks;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.tools.TextureUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Block26L extends Block {

	public Block26L(MapPart mapchunk, BlockInfo block, Vector3 pos, int direction) {
		super(mapchunk, block, pos, direction, 0);
	}

	@Override
	public void initPoints() {
		float nsize = size / 2;
		points[2].y = nsize;
		points[3].y = nsize;
		points[6].y = nsize;
		points[7].y = nsize;
	}

	@Override
	public void addLid(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.lid);
		if(direction == 0) addVertex(0, 1, 7, 6, texCoords, -1);
		if(direction == 1) addVertex(3, 2, 4, 5, texCoords, -1);
		if(direction == 2) addVertex(0, 2, 7, 5, texCoords, -1);
		if(direction == 3) addVertex(3, 1, 4, 6, texCoords, -1);
	}

	@Override
	public void addTop(BlockInfo block) {
		Vector2[] texCoords;
		if(direction == 0 || direction == 1) {
			texCoords = TextureUtils.calculateUVMapping(block.top);
			addVertex(4, 5, 6, 7, texCoords, UP);
		}
		if(direction == 2) {
			texCoords = TextureUtils.calculateUVMapping(block.top);
			slopeOffset(texCoords, 2, block.top, 1f/32);
			slopeOffset(texCoords, 3, block.top, 0.5f/32);
			addVertex(4, 5, 5, 7, texCoords, UP);
		}
		if(direction == 3) {
			texCoords = TextureUtils.calculateUVMapping(block.top);
			slopeOffset(texCoords, 2, block.top, 0.5f/32);
			slopeOffset(texCoords, 3, block.top, 0.5f/32);
			addVertex(4, 5, 6, 4, texCoords, UP);
		}
	}

	@Override
	public void addBottom(BlockInfo block) {
		Vector2[] texCoords;
		if(direction == 0 || direction == 1) {
			texCoords = TextureUtils.calculateUVMapping(block.bottom);
			addVertex(0, 1, 2, 3, texCoords, DOWN);
		}
		if(direction == 2) {
			texCoords = TextureUtils.calculateUVMapping(block.bottom);
			slopeOffset(texCoords, 2, block.bottom, 0.5f/32);
			slopeOffset(texCoords, 3, block.bottom, 0.5f/32);
			addVertex(0, 1, 2, 0, texCoords, DOWN);
		}
		if(direction == 3) {
			texCoords = TextureUtils.calculateUVMapping(block.bottom);
			slopeHOffset(texCoords, 2, block.bottom, 1f/32);
			slopeOffset(texCoords, 2, block.bottom, 0.5f/32);
			slopeOffset(texCoords, 3, block.bottom, 0.5f/32);
			addVertex(0, 1, 3, 3, texCoords, DOWN);
		}
	}

	@Override
	public void addRight(BlockInfo block) {
		Vector2[] texCoords;
		if(direction == 0) {
			texCoords = TextureUtils.calculateUVMapping(block.right);
			slopeOffset(texCoords, 2, block.right, 0.5f/32);
			slopeOffset(texCoords, 3, block.right, 0.5f/32);
			addVertex(1, 4, 7, 1, texCoords, RIGHT);
		}
		if(direction == 1) {
			texCoords = TextureUtils.calculateUVMapping(block.right);
			slopeOffset(texCoords, 2, block.right, 1f/32);
			slopeOffset(texCoords, 3, block.right, 0.5f/32);
			addVertex(1, 4, 4, 2, texCoords, RIGHT);
		}
		if(direction == 2 || direction == 3) {
			texCoords = TextureUtils.calculateUVMapping(block.right);
			addVertex(1, 4, 7, 2, texCoords, RIGHT);
		}
	}

	@Override
	public void addLeft(BlockInfo block) {
		Vector2[] texCoords;
		if(direction == 0) {
			texCoords = TextureUtils.calculateUVMapping(block.left);
			slopeOffset(texCoords, 2, block.left, 1f/32);
			slopeOffset(texCoords, 3, block.left, 0.5f/32);
			addVertex(5, 0, 0, 6, texCoords, LEFT);
		}
		if(direction == 1) {
			texCoords = TextureUtils.calculateUVMapping(block.left);
			slopeOffset(texCoords, 2, block.left, 0.5f/32);
			slopeOffset(texCoords, 3, block.left, 0.5f/32);
			addVertex(5, 0, 3, 5, texCoords, LEFT);
		}
		if(direction == 2 || direction == 3) {
			texCoords = TextureUtils.calculateUVMapping(block.left);
			addVertex(5, 0, 3, 6, texCoords, LEFT);
		}
	}

}
