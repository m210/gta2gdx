//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map.blocks;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.tools.TextureUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class PartialBlock extends Block {

	public PartialBlock(MapPart mapchunk, BlockInfo block, Vector3 pos, int direction) {
		super(mapchunk, block, pos, direction, 0);
	}

	@Override
	public void initPoints() {
		float nsize = size / 3;
		if(direction == 0 || direction == 4 || direction == 5) {
			points[0].z -= 2 * nsize;
			points[1].z -= 2 * nsize;
			points[2].z -= 2 * nsize;
			points[3].z -= 2 * nsize;
		}
		if(direction == 1 || direction == 6 || direction == 7) {
			points[6].z += 2 * nsize;
			points[7].z += 2 * nsize;
			points[5].z += 2 * nsize;
			points[4].z += 2 * nsize;
		}
		if(direction == 2 || direction == 4 || direction == 7) {
			points[1].x -= 2 * nsize;
			points[2].x -= 2 * nsize;
			points[4].x -= 2 * nsize;
			points[7].x -= 2 * nsize;
		}
		if(direction == 3 || direction == 5 || direction == 6) {
			points[6].x += 2 * nsize;
			points[5].x += 2 * nsize;
			points[3].x += 2 * nsize;
			points[0].x += 2 * nsize;
		}
		if(direction == 8) {
			points[6].x += nsize;
			points[5].x += nsize;
			points[3].x += nsize;
			points[0].x += nsize;

			points[1].x -= nsize;
			points[2].x -= nsize;
			points[4].x -= nsize;
			points[7].x -= nsize;

			points[6].z += nsize;
			points[7].z += nsize;
			points[5].z += nsize;
			points[4].z += nsize;

			points[0].z -= nsize;
			points[1].z -= nsize;
			points[2].z -= nsize;
			points[3].z -= nsize;
		}
	}

	@Override
	public void addLid(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.lid);
		if(direction == 0 || direction == 4 || direction == 5) {
			slopeOffset(texCoords, 0, block.lid, -1f/48);
			slopeOffset(texCoords, 1 , block.lid, -1f/48);
		}
		if(direction == 1 || direction == 6 || direction == 7) {
			slopeOffset(texCoords, 2, block.lid, 1f/48);
			slopeOffset(texCoords, 3 , block.lid, 1f/48);
		}
		if(direction == 2 || direction == 4 || direction == 7) {
			slopeHOffset(texCoords, 1, block.lid, 1f/48);
			slopeHOffset(texCoords, 2 , block.lid, 1f/48);
		}
		if(direction == 3 || direction == 5 || direction == 6) {
			slopeHOffset(texCoords, 0, block.lid, -1f/48);
			slopeHOffset(texCoords, 3 , block.lid, -1f/48);
		}
		if(direction == 8) {
			slopeHOffset(texCoords, 1, block.lid, 1f/48);
			slopeHOffset(texCoords, 2 , block.lid, 1f/48);
			slopeHOffset(texCoords, 0, block.lid, -1f/48);
			slopeHOffset(texCoords, 3 , block.lid, -1f/48);

			slopeOffset(texCoords, 0, block.lid, -1f/48);
			slopeOffset(texCoords, 1 , block.lid, -1f/48);
			slopeOffset(texCoords, 2, block.lid, 1f/48);
			slopeOffset(texCoords, 3 , block.lid, 1f/48);
			addVertex(7, 6, 3, 2, texCoords, -1);
		} else
			addVertex(3, 2, 7, 6, texCoords, -1);
	}

	@Override
	public void addTop(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.top);
		if(direction == 2 || direction == 4 || direction == 7) {
			slopeHOffset(texCoords, 0, block.top, -1f/48);
			slopeHOffset(texCoords, 3 , block.top, -1f/48);
		}
		if(direction == 3 || direction == 5 || direction == 6) {
			slopeHOffset(texCoords, 1, block.top, 1f/48);
			slopeHOffset(texCoords, 2, block.top, 1f/48);
		}
		if(direction == 8) {
			float offset = -1f/48;
			slopeHOffset(texCoords, 1, block.top, 1f/96);
			slopeHOffset(texCoords, 2, block.top, 1f/96);
			slopeHOffset(texCoords, 0, block.top, offset + 1f/96);
			slopeHOffset(texCoords, 3, block.top, offset + 1f/96);
		}
		addVertex(4, 5, 6, 7, texCoords, UP);
	}

	@Override
	public void addBottom(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.bottom);
		if(direction == 2  || direction == 4 || direction == 7) {
			slopeHOffset(texCoords, 1, block.bottom, 1f/48);
			slopeHOffset(texCoords, 2 , block.bottom, 1f/48);
		}
		if(direction == 3 || direction == 5 || direction == 6) {
			slopeHOffset(texCoords, 0, block.bottom, -1f/48);
			slopeHOffset(texCoords, 3, block.bottom, -1f/48);
		}
		if(direction == 8) {
			float offset = -1f/48;
			slopeHOffset(texCoords, 1, block.bottom, 1f/96);
			slopeHOffset(texCoords, 2, block.bottom, 1f/96);
			slopeHOffset(texCoords, 0, block.bottom, offset + 1f/96);
			slopeHOffset(texCoords, 3, block.bottom, offset + 1f/96);

		}
		addVertex(0, 1, 2, 3, texCoords, DOWN);
	}

	@Override
	public void addRight(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.right);
		if(direction == 0 || direction == 4) {
			slopeHOffset(texCoords, 0, block.right, -1f/48);
			slopeHOffset(texCoords, 3 , block.right, -1f/48);
		}
		if(direction == 5) {
			slopeHOffset(texCoords, 0, block.right, -1f/48);
			slopeHOffset(texCoords, 3 , block.right, -1f/48);
		}
		if(direction == 1 || direction == 6 || direction == 7) {
			slopeHOffset(texCoords, 1, block.right, 1f/48);
			slopeHOffset(texCoords, 2 , block.right, 1f/48);
		}
		if(direction == 8) {
			float offset = -1f/48;
			slopeHOffset(texCoords, 1, block.right, 1f/96);
			slopeHOffset(texCoords, 2, block.right, 1f/96);
			slopeHOffset(texCoords, 0, block.right, offset + 1f/96);
			slopeHOffset(texCoords, 3, block.right, offset + 1f/96);
		}
		addVertex(1, 4, 7, 2, texCoords, RIGHT);
	}

	@Override
	public void addLeft(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.left);
		if(direction == 0 || direction == 4) {
			slopeHOffset(texCoords, 1, block.left, 1f/48);
			slopeHOffset(texCoords, 2 , block.left, 1f/48);
		}
		if(direction == 5) {
			slopeHOffset(texCoords, 1, block.left, 1f/48);
			slopeHOffset(texCoords, 2 , block.left, 1f/48);
		}
		if(direction == 1 || direction == 6 || direction == 7) {
			slopeHOffset(texCoords, 0, block.left, -1f/48);
			slopeHOffset(texCoords, 3 , block.left, -1f/48);
		}
		if(direction == 8) {
			float offset = -1f/48;
			slopeHOffset(texCoords, 1, block.left, 1f/96);
			slopeHOffset(texCoords, 2, block.left, 1f/96);
			slopeHOffset(texCoords, 0, block.left, offset + 1f/96);
			slopeHOffset(texCoords, 3, block.left, offset + 1f/96);
		}
		addVertex(5, 0, 3, 6, texCoords, LEFT);
	}
}
