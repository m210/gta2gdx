//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map.blocks;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.tools.TextureUtils;

public class DiagonalFacingBlock extends Block {

	public DiagonalFacingBlock(MapPart mapchunk, BlockInfo block, Vector3 pos, int direction) {
		super(mapchunk, block, pos, direction, 0);
	}

	@Override
	public void addLid(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.lid);
		if(direction == 0)
			addVertex(3, 2, 7, 7, texCoords, -1);
		if(direction == 1) {
			texCoords[2] = texCoords[3];
			addVertex(3, 2, 6, 6, texCoords, -1);
		}
		if(direction == 2) {
			texCoords[0] = texCoords[3];
			addVertex(6, 2, 7, 6, texCoords, -1);
		}
		if(direction == 3)
			addVertex(3, 7, 7, 6, texCoords, -1);
	}

	@Override
	public void addTop(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.top);
		addVertex(4, 5, 6, 7, texCoords, UP);
	}

	@Override
	public void addBottom(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.bottom);
		addVertex(0, 1, 2, 3, texCoords, DOWN);
	}

	@Override
	public void addRight(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.right);
		if(direction == 1) addVertex(1, 5, 6, 2, texCoords, RIGHT);
		if(direction == 3) addVertex(0, 4, 7, 3, texCoords, RIGHT);
		if(direction == 0 || direction == 2) addVertex(1, 4, 7, 2, texCoords, RIGHT);
	}

	@Override
	public void addLeft(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.left);
		if(direction == 0) addVertex(4, 0, 3, 7, texCoords, LEFT);
		if(direction == 2) addVertex(5, 1, 2, 6, texCoords, LEFT);
		if(direction == 1 || direction == 3) addVertex(5, 0, 3, 6, texCoords, LEFT);
	}

	@Override
	public void initPoints() {

	}

}
