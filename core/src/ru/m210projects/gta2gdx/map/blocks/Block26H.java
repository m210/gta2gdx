//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map.blocks;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.tools.TextureUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Block26H extends Block {
	float nsize;

	public Block26H(MapPart mapchunk, BlockInfo block, Vector3 pos, int direction) {
		super(mapchunk, block, pos, direction, 0);
	}

	@Override
	public void initPoints() {
		nsize = size / 2;
		if(direction == 0) {
			points[3].y = nsize;
			points[2].y = nsize;
		}
		if(direction == 1) {
			points[6].y = nsize;
			points[7].y = nsize;
		}
		if(direction == 2) {
			points[3].y = nsize;
			points[6].y = nsize;
		}
		if(direction == 3) {
			points[2].y = nsize;
			points[7].y = nsize;
		}
	}

	@Override
	public void addLid(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.lid);
		addVertex(3, 2, 7, 6, texCoords, -1);
	}

	@Override
	public void addTop(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.top);
		if(direction == 2) slopeOffset(texCoords, 2, block.top, nsize / 32);
		if(direction == 3) slopeOffset(texCoords, 3, block.top, nsize / 32);
		addVertex(4, 5, 6, 7, texCoords, UP);
	}

	@Override
	public void addBottom(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.bottom);
		if(direction == 2) slopeOffset(texCoords, 3, block.bottom, nsize / 32);
		if(direction == 3) slopeOffset(texCoords, 2, block.bottom, nsize / 32);
		addVertex(0, 1, 2, 3, texCoords, DOWN);
	}

	@Override
	public void addLeft(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.left);
		if(direction == 0) slopeOffset(texCoords, 2, block.left, nsize / 32);
		if(direction == 1) slopeOffset(texCoords, 3, block.left, nsize / 32);
		addVertex(5, 0, 3, 6, texCoords, LEFT);
	}

	@Override
	public void addRight(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.right);
		if(direction == 0) slopeOffset(texCoords, 3, block.right, nsize / 32);
		if(direction == 1) slopeOffset(texCoords, 2, block.right, nsize / 32);
		addVertex(1, 4, 7, 2, texCoords, RIGHT);
	}

}
