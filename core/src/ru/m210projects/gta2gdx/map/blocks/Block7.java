//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map.blocks;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.tools.TextureUtils;

public class Block7 extends Block {
	float h1;
	float h2;

	public Block7(MapPart mapchunk, BlockInfo block, Vector3 pos, int direction, int h) {
		super(mapchunk, block, pos, direction, h);
	}

	@Override
	public void initPoints() {
		h1 = 0.125f * h - 0.125f;
		h2 = size * 0.125f * h;

        if(direction == 0) {
			points[2].y = h1;
			points[3].y = h1;
			points[6].y = h2;
			points[7].y = h2;
        }

        if(direction == 1) {
			points[2].y = h2;
			points[3].y = h2;
			points[6].y = h1;
			points[7].y = h1;
        }
        if(direction == 2) {
        	points[2].y = h2;
			points[3].y = h1;
			points[6].y = h1;
			points[7].y = h2;
        }
        if(direction == 3) {
        	points[2].y = h1;
			points[3].y = h2;
			points[6].y = h2;
			points[7].y = h1;
        }
	}

	@Override
	public void addLid(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.lid);
		addVertex(3, 2, 7, 6, texCoords, -1);
	}

	@Override
	public void addTop(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.top);
		if(direction == 2) {
			slopeOffset(texCoords, 3, block.top, 0.125f / 32 * (8 - h));
			slopeOffset(texCoords, 2, block.top, 0.125f / 32 * (9 - h));
		}
		if(direction == 3) {
			slopeOffset(texCoords, 2, block.top, 0.125f / 32 * (8 - h));
			slopeOffset(texCoords, 3, block.top, 0.125f / 32 * (9 - h));
		}
		addVertex(4, 5, 6, 7, texCoords, UP);
	}

	@Override
	public void addBottom(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.bottom);
		if(direction == 2) {
			slopeOffset(texCoords, 3, block.bottom, 0.125f / 32 * (9 - h));
			slopeOffset(texCoords, 2, block.bottom, 0.125f / 32 * (8 - h));
		}
		if(direction == 3) {
			slopeOffset(texCoords, 2, block.bottom, 0.125f / 32 * (9 - h));
			slopeOffset(texCoords, 3, block.bottom, 0.125f / 32 * (8 - h));
		}
		addVertex(0, 1, 2, 3, texCoords, DOWN);
	}

	@Override
	public void addLeft(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.left);
		if(direction == 0) {
			slopeOffset(texCoords, 3, block.left, 0.125f / 32 * (8 - h));
			slopeOffset(texCoords, 2, block.left, 0.125f / 32 * (9 - h));
		}
		if(direction == 1) {
			slopeOffset(texCoords, 2, block.left, 0.125f / 32 * (8 - h));
			slopeOffset(texCoords, 3, block.left, 0.125f / 32 * (9 - h));
		}

		addVertex(5, 0, 3, 6, texCoords, LEFT);
	}

	@Override
	public void addRight(BlockInfo block) {
		Vector2[] texCoords = TextureUtils.calculateUVMapping(block.right);
		if(direction == 0) {
			slopeOffset(texCoords, 3, block.right, 0.125f / 32 * (9 - h));
			slopeOffset(texCoords, 2, block.right, 0.125f / 32 * (8 - h));
		}
		if(direction == 1) {
			slopeOffset(texCoords, 2, block.right, 0.125f / 32 * (9 - h));
			slopeOffset(texCoords, 3, block.right, 0.125f / 32 * (8 - h));
		}
		addVertex(1, 4, 7, 2, texCoords, RIGHT);
	}
}
