//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map.blocks;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.map.MapPart.MeshType;
import ru.m210projects.gta2gdx.tools.BitHelper;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public abstract class Block {

	public static final int UP = 0;
	public static final int DOWN = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	public static final int LID = -1;

	protected static final float size = BlockInfo.SIZE;
	protected final static int vertexSize = 9; //pos * 3 + tex * 2 + color * 4

//	public static final int MAXVERTICES = World.PARTSIZE * World.PARTSIZE * 8 * 20 * vertexSize; 		//251136 - max mesh on map
//	public static final int MAXINDICIES = World.PARTSIZE * World.PARTSIZE * 8 * 30; 					//41856

	protected BlockInfo block;
	protected Vector3 pos;
	protected int direction;
	protected int h;

//	protected final float[] topNormal = new float[]{0.0f, 0.0f, -1.0f};
//	protected final float[] bottomNormal = new float[]{0.0f, 0.0f, 1.0f};
//	protected final float[] rightNormal = new float[]{1.0f, 0.0f, 0.0f};
//	protected final float[] leftNormal = new float[]{-1.0f, 0.0f, 0.0f};
//	protected final float[] lidNormal = new float[]{0.0f, 1.0f, 0.0f};

	protected Vector3[] points = {
		new Vector3(0, 0, size), // 0
		new Vector3(size, 0, size), // 1
		new Vector3(size, size, size), //2
		new Vector3(0, size, size), //3
		new Vector3(size, 0, 0), //4
		new Vector3(0, 0, 0), //5
		new Vector3(0, size, 0), //6
		new Vector3(size, size, 0) //7
	};

	protected final MapPart chunk;
	public Block(MapPart mapchunk, BlockInfo block, Vector3 pos, int direction, int h) {
		this.block = block;
		this.pos = pos;
		this.chunk = mapchunk;
		this.direction = direction;
		this.h = h;
		initPoints();
		addSurfaces(block);
	}

	public abstract void initPoints();
	public abstract void addLid(BlockInfo block);
	public abstract void addTop(BlockInfo block);
	public abstract void addBottom(BlockInfo block);
	public abstract void addRight(BlockInfo block);
	public abstract void addLeft(BlockInfo block);

	public void addSurfaces(BlockInfo block) {
		if(getTileID(block.lid) != 0) addLid(block);
    	if(getTileID(block.top) != 0) addTop(block);
    	if(getTileID(block.bottom) != 0) addBottom(block);
    	if(getTileID(block.left) != 0) addLeft(block);
    	if(getTileID(block.right) != 0) addRight(block);
	}

	public void addVertex(int a, int b, int c, int d, Vector2[] texCoords, int surface) {
		boolean flat = false;
		boolean flat2 = false;
		float aOffset = 0.999f;

		if(block.slope_type >=45 && block.slope_type <= 52)
			aOffset = 0;

		float posx = pos.x;
		float posy = pos.y;
		float posz = pos.z;

		if(surface == LID)
			flat = BitHelper.CheckBit(block.lid, 12);

		if(surface == UP) {
			flat = BitHelper.CheckBit(block.top, 12);
			flat2 = BitHelper.CheckBit(block.bottom, 12);

			if(block.slope_type >= 55 && block.slope_type <= 61)
				aOffset = 0.333f;

			if(!flat && flat2) posy += aOffset;
		}
		if(surface == DOWN) {
			flat2 = BitHelper.CheckBit(block.top, 12);
			flat = BitHelper.CheckBit(block.bottom, 12);

			if(block.slope_type >= 55 && block.slope_type <= 61)
				aOffset = 0.333f;

			if(!flat && flat2) posy -= aOffset;
		}
		if(surface == LEFT) {
			flat = BitHelper.CheckBit(block.left, 12);
			flat2 = BitHelper.CheckBit(block.right, 12);

			if(block.slope_type == 53 || block.slope_type == 54)
				aOffset = 0.333f;

			if(block.slope_type >= 57 && block.slope_type <= 61)
				aOffset = 0.333f;

			if(!flat && flat2) posx += aOffset;
		}
		if(surface == RIGHT) {
			flat2 = BitHelper.CheckBit(block.left, 12);
			flat = BitHelper.CheckBit(block.right, 12);

			if(block.slope_type == 53 || block.slope_type == 54)
				aOffset = 0.333f;

			if(block.slope_type >= 57 && block.slope_type <= 61)
				aOffset = 0.333f;

			if(!flat && flat2) posx -= aOffset;
		}
		MeshType type = MeshType.Solid;
		if(flat || flat2)
			type = MeshType.Alpha;

		int vertexOffset = chunk.getVertices(type).size / vertexSize;
		for(int vx = 0; vx < 4; vx++) {
	    	int point = d;
	    	if(vx == 0) point = a; else
	    	if(vx == 1) point = b; else
	    	if(vx == 2) point = c;

	    	chunk.getVertices(type).addAll(points[point].x + posx, points[point].y + posz, points[point].z + posy, texCoords[vx].x, texCoords[vx].y, 1, 1, 1, 1);
		}
    	chunk.getIndicies(type).addAll((short) (vertexOffset), (short) (1 + vertexOffset), (short) (2 + vertexOffset), (short) (2 + vertexOffset), (short) (3 + vertexOffset), (short) (vertexOffset));

    	if((block.slope_type < 45 || block.slope_type > 52) && flat && flat2) {
    		if(surface == UP)
    			posy += aOffset;
    		if(surface == DOWN)
    			posy -= aOffset;
    		if(surface == LEFT)
    			posx += aOffset;
    		if(surface == RIGHT)
    			posx -= aOffset;

	    	vertexOffset = chunk.getVertices(type).size / vertexSize;
			for(int vx = 0; vx < 4; vx++) {
		    	int point = d;
		    	if(vx == 0) point = a; else
		    	if(vx == 1) point = b; else
		    	if(vx == 2) point = c;

		    	chunk.getVertices(type).addAll(points[point].x + posx, points[point].y + posz, points[point].z + posy, texCoords[vx].x, texCoords[vx].y, 1, 1, 1, 1);
			}
	    	chunk.getIndicies(type).addAll((short) (vertexOffset), (short) (1 + vertexOffset), (short) (2 + vertexOffset), (short) (2 + vertexOffset), (short) (3 + vertexOffset), (short) (vertexOffset));
    	}
	}

	private int getTileID(int tileid) {
		tileid &= ~(1 << 10 | 1 << 11 | 1 << 12 | 1 << 13 | 1 << 14 | 1 << 15) & 0xffff;
		return tileid;
	}

	public Vector2[] slopeOffset(Vector2[] UVList, int point, int texture, float slope_offset) {
		boolean xOrin = BitHelper.CheckBit(texture, 14);
	    boolean yOrin = BitHelper.CheckBit(texture, 15);

	    if(xOrin && !yOrin)
	    	UVList[point].x += slope_offset;
	    if(!xOrin && yOrin)
	    	UVList[point].y -= slope_offset;
	    if(!xOrin && !yOrin)
	    	UVList[point].y += slope_offset;
	    if(xOrin && yOrin)
	    	UVList[point].x -= slope_offset;

		return UVList;
	}

	public Vector2[] slopeHOffset(Vector2[] UVList, int point, int texture, float slope_offset) {
		boolean xOrin = BitHelper.CheckBit(texture, 14);
	    boolean yOrin = BitHelper.CheckBit(texture, 15);

	    if(BitHelper.CheckBit(texture, 13))
	    	slope_offset *= -1;

	    if(xOrin && !yOrin)
	    	UVList[point].y += slope_offset;
	    if(!xOrin && yOrin)
	    	UVList[point].x += slope_offset;
	    if(!xOrin && !yOrin)
	    	UVList[point].x -= slope_offset;
	    if(xOrin && yOrin)
	    	UVList[point].y -= slope_offset;

		return UVList;
	}
}