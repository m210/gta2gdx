//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.tools.IOStream;
import ru.m210projects.gta2gdx.tools.LittleEndian;

public class GmpReader {
	private IOStream io;
	private int handle;

	private int chunkData;
	private String chunkHeader;
	private boolean debug = false;

	static Map<String, Integer> chunkOffset = new HashMap<String, Integer>();
	static Map<String, Integer> chunkSize = new HashMap<String, Integer>();


	public GmpReader(String path) {
		io = new IOStream();
		handle = init(path);
	}

	public int init(String path) {
		int fis = io.open(path);

		byte[] buf = new byte[4];

		io.read(fis, buf, buf.length);
		chunkHeader = new String(buf);
		if (!chunkHeader.equals("GBMP")) {
			log("Not a GBH/GTA2 map!");
			return -1;
		}

		buf = new byte[2];
		io.read(fis, buf, buf.length);
		chunkData = LittleEndian.getShort(buf);

		if (chunkData != 500) {
			log("Bad GTA2 map version (must be 500) " + chunkData);
			log("Will ignore that for now, and read as if it was version 500...");
		}

		int offset = 0;
		buf = new byte[4];
		while(io.available(fis) > 0) {
			io.read(fis, buf, buf.length);
			chunkHeader = new String(buf);
			io.read(fis, buf, buf.length);
			offset = io.getcursor(fis);
			chunkData = LittleEndian.getInt(buf);
			log(chunkHeader);
			chunkOffset.put(chunkHeader, offset);
			chunkSize.put(chunkHeader, chunkData);
			io.lseek(fis, chunkData, IOStream.SEEK_CUR);
		}

		return fis;
	}

	public BlockInfo[][][] chunk_dmap() {
		io.lseek(handle, chunkOffset("DMAP"), IOStream.SEEK_SET);
		chunkData = chunkData("DMAP");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);

		log("DMAP: Reading 32-bit compressed map...");


		BlockInfo[][][] city_scape = new BlockInfo[8][256][256];
		int[][] base = new int[256][256];

		int column_words;
		int[] column;
		int num_blocks;
		BlockInfo[] blocks;

		//read base
		int pos = 0;
		for (int i = 0; i < 256; i++) {
			for (int j = 0; j < 256; j++) {
				base[i][j] = LittleEndian.getInt(buf, pos);
				pos += 4;
			}
		}

		//read column words
		column_words = LittleEndian.getInt(buf, pos);
		pos += 4;
		log("DMAP: Reading columns..." + column_words);
		column = new int[column_words];

		for (int i = 0; i < column_words; i++) {
			column[i] = LittleEndian.getInt(buf, pos);
			pos += 4;
		}
		//read blocks
		num_blocks = LittleEndian.getInt(buf, pos);
		pos += 4;
		log("DMAP: Reading blocks..." + num_blocks);

		blocks = new BlockInfo[num_blocks];
		for (int i = 0; i < num_blocks; i++) {
			short left = LittleEndian.getShort(buf, pos);
			pos += 2;
			short right = LittleEndian.getShort(buf, pos);
			pos += 2;
			short top = LittleEndian.getShort(buf, pos);
			pos += 2;
			short bottom = LittleEndian.getShort(buf, pos);
			pos += 2;
			short lid = LittleEndian.getShort(buf, pos);
			pos += 2;
			byte arrows = buf[pos++];
			byte slope_type = buf[pos++];
			blocks[i] = new BlockInfo(left, right, top, bottom, lid, arrows, slope_type);
		}

		//compute the map
		log("DMAP: Creating city scape...");
		for (int i = 0; i < 256; i++) {
			for (int j = 0; j < 256; j++) {
				Resources.groundz[i][j] = -1;
				Resources.roadz[i][j] = -1;
				int col = base[j][i];
				byte height = (byte) (column[col] & 0xFF);
				byte offset = (byte) ((column[col] & 0xFF00) >> 8);

				for (int bk = 0; bk < height; bk++) {
					if (bk >= offset) {
						BlockInfo curr_block = blocks[column[col + bk - offset + 1]];

						if(curr_block.ground_type == BlockInfo.PAVEMENT)
							Resources.groundz[i][j] = bk;
						if(curr_block.ground_type == BlockInfo.ROAD)
							Resources.roadz[i][j] = bk;

						city_scape[bk][i][j] = curr_block;
					}
				}
			}
		}

		blocks = null;
		column = null;
		base = null;

		if(debug) {
			int dsize = 256*256*10*12;
			int to = (chunkData * 100 / dsize);
			log("DMAP: Uncompressed from " + chunkData + " bytes to " + dsize + " bytes (" + to + "% ratio)");
		}

		return city_scape;
	}

	public List<Zone> chunk_zones() {
		log("ZONE: Scanning zones");
		io.lseek(handle, chunkOffset("ZONE"), IOStream.SEEK_SET);
		chunkData = chunkData("ZONE");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);

		List<Zone> zones = new ArrayList<Zone>();

		int numzones = 0;
		int zone_type, x, y, w, h, name_length;
		for(int i = 0; i < buf.length;) {
			zone_type = buf[i++] & 0xFF;
			x = buf[i++] & 0xFF;
			y = buf[i++] & 0xFF;
			w = buf[i++] & 0xFF;
			h = buf[i++] & 0xFF;
			name_length = buf[i++] & 0xFF;

			String name = Character.toString((char)buf[i++]);
			for(int n = 1; n < name_length; n++)
				name += Character.toString((char)buf[i++]);

			zones.add(new Zone(zone_type, x, y, w, h, name));
			log(name);
			numzones++;

		}
		log("ZONE: Found " + numzones + " zones");
		return zones;
	}

	public List<Object> chunk_mobj() {
		log("MOBJ: Found " + (chunkData / 6) + " map objects");
		io.lseek(handle, chunkOffset("MOBJ"), IOStream.SEEK_SET);
		chunkData = chunkData("MOBJ");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		List<Object> mobj = new ArrayList<Object>();

		int x, y, ang, type;

		for (int i = 0; i < buf.length;) {
			x = LittleEndian.getShort(buf, i);
			i += 2;
			y = LittleEndian.getShort(buf, i);
			i += 2;
			ang = LittleEndian.getUByte(buf, i++);
			type = LittleEndian.getUByte(buf, i++);
			mobj.add(new Object(x, y, ang, type));
		}
		return mobj;
	}

	public List<Animation> chunk_anim() {
		log("ANIM: Scanning animation tiles");
		io.lseek(handle, chunkOffset("ANIM"), IOStream.SEEK_SET);
		chunkData = chunkData("ANIM");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		List<Animation> anim = new ArrayList<Animation>();

		short base, tiles[];
		byte frame_rate, repeat;
		int anim_length;
		for(int i = 0; i < buf.length;) {
			base = LittleEndian.getShort(buf, i);
			i += 2;
			frame_rate = buf[i++];
			repeat = buf[i++];
			anim_length = buf[i++] & 0xFF;
			i++;
			tiles = new short[anim_length];
			for (int a = 0; a < anim_length; a++) {
				tiles[a] = LittleEndian.getShort(buf, i);
				i += 2;
			}
			anim.add(new Animation(base, frame_rate, repeat, tiles));
		}
		log("ANIM: Found " + anim.size() + " animations");
		return anim;
	}

	public void chunk_lght() {
		log("LGHT: Found " + (chunkData / 16) + " light sources");
		io.lseek(handle, chunkOffset("LGHT"), IOStream.SEEK_SET);
		chunkData = chunkData("LGHT");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);

		short x, y, z, rad, blue, red, green;
		byte intensity, shape, on_time, off_time;

		for(int i = 0; i < buf.length;) {
			blue = LittleEndian.getUByte(buf, i++);
			green = LittleEndian.getUByte(buf, i++);
			red = LittleEndian.getUByte(buf, i++);
			i++; //alpha

			x = LittleEndian.getShort(buf, i);
			i+=2;
			y = LittleEndian.getShort(buf, i);
			i+=2;
			z = LittleEndian.getShort(buf, i);
			i+=2;
			rad = LittleEndian.getShort(buf, i);
			i+=2;
			intensity = buf[i++];
			shape = buf[i++];
			on_time = buf[i++];
			off_time = buf[i++];

			float offsetX = x % 128 / 127.0f;
			float offsetY = y % 128 / 127.0f;
			float offsetZ = z % 128 / 127.0f;

			x = (short) (((x & 0xFF80) >> 7) % 256 + (x & 0x7F) / 128.0f + World.PARTSIZE);
			y = (short) (((y & 0xFF80) >> 7) % 256 + (y & 0x7F) / 128.0f + World.PARTSIZE);
			z = (short) (((z & 0xFF80) >> 7) % 256 + (z & 0x7F) / 128.0f);
			rad = (short) (((rad & 0xFF80) >> 7) % 256 + (rad & 0x7F) / 128.0f);

			Light light = new Light(x + offsetX, y + offsetY, z + offsetZ, red, green, blue, rad, intensity, shape, on_time, off_time);

			int ch_x = (int) (light.getPosition().x / World.PARTSIZE);
			int ch_y = (int) (light.getPosition().y / World.PARTSIZE);
			int chunkNum = World.MAPPARTS * ch_x + ch_y;
			MapPart[] chunks = Resources.getParts();
			chunks[chunkNum].addLight(light);

			byte bit1 = 1;
			byte bit2 = 1;

			int cx = (chunkNum / World.MAPPARTS) * World.PARTSIZE;
			int cy = (chunkNum % World.MAPPARTS) * World.PARTSIZE;

			if(light.getPosition().x >= cx && light.getPosition().x < cx + World.PARTSIZE / 2)
				bit1 = -1;
			if(light.getPosition().y >= cy && light.getPosition().y < cy + World.PARTSIZE / 2)
				bit2 = -1;

			if(light.getPosition().x - rad < cx && light.getPosition().y - rad < cy ||
			   light.getPosition().x + rad > cx + World.PARTSIZE && light.getPosition().y + rad > cy + World.PARTSIZE ||
			   light.getPosition().x - rad < cx && light.getPosition().y + rad > cy + World.PARTSIZE ||
			   light.getPosition().x + rad > cx + World.PARTSIZE && light.getPosition().y - rad < cy)
				chunks[chunkNum + World.MAPPARTS * bit1 + bit2].addLight(light);

			if(light.getPosition().x - rad < cx || light.getPosition().x + rad > cx + World.PARTSIZE)
				chunks[chunkNum + World.MAPPARTS * bit1].addLight(light);
			if(light.getPosition().y - rad < cy || light.getPosition().y + rad > cy + World.PARTSIZE)
				chunks[chunkNum + bit2].addLight(light);
		}
	}

	public void close() {
		io.close(handle);
	}

	private int chunkOffset(String chunk) {
		int out = 0;
		for(Map.Entry<String, Integer> entry : chunkOffset.entrySet()) {
			if(entry.getKey().equals(chunk)) {
				out = entry.getValue();
			}
		}
		return out;
	}

	private int chunkData(String chunk) {
		int out = 0;
		for(Map.Entry<String, Integer> entry : chunkSize.entrySet()) {
			if(entry.getKey().equals(chunk)) {
				out = entry.getValue();
			}
		}
		return out;
	}

	private void log(String txt) {
		if(debug)
			System.out.println(txt);
	}
}
