//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map;

import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.math.Vector3;

public class Light {
	private final PointLight light;
	private final Vector3 pos;
	//private byte shape, on_time, off_time;

	public Light(float x, float y, float z, short red, short green, short blue, float radius, int intensity, byte shape, byte on_time, byte off_time) {
		pos = new Vector3(x, y, z);
		intensity = intensity & 0xff;
		light = new PointLight().set((red * intensity) / 65025.0f, (green * intensity) / 65025.0f, (blue * intensity) / 65025.0f, x, z, y, radius);
	}

	public Vector3 getPosition() {
		return pos;
	}

	public PointLight getLight() {
		return light;
	}
}
