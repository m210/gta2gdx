//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.map;


import java.util.ArrayList;
import java.util.List;

import ru.m210projects.gta2gdx.Renderer;
import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.map.blocks.Block;
import ru.m210projects.gta2gdx.map.blocks.Block26H;
import ru.m210projects.gta2gdx.map.blocks.Block26L;
import ru.m210projects.gta2gdx.map.blocks.Block45;
import ru.m210projects.gta2gdx.map.blocks.Block7;
import ru.m210projects.gta2gdx.map.blocks.CubeBlock;
import ru.m210projects.gta2gdx.map.blocks.DiagonalFacingBlock;
import ru.m210projects.gta2gdx.map.blocks.DiagonalSlopeBlock;
import ru.m210projects.gta2gdx.map.blocks.PartialBlock;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ShortArray;

public class MapPart {

	public static final int TOP = 0;
	public static final int BOTTOM = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;

	public enum MeshType {
		Solid,
		Alpha,
	}
	private Mesh mesh;
	private Mesh amesh;
	private Vector2 pos;
	public int partNum;

	private FloatArray vertices;
	private ShortArray indicies;

	private FloatArray avertices;
	private ShortArray aindicies;

	private List<Light> lights = new ArrayList<Light>();

	public MapPart(int partNum) {
		this.partNum = partNum;
	}

	public void init(Vector2 pos) {
		vertices = new FloatArray();
	    indicies = new ShortArray();

	    avertices = new FloatArray();
	    aindicies = new ShortArray();

	    for(int z = 0; z < 8; z++) {
	        for(int x = (int) pos.x; x < (int) pos.x + World.PARTSIZE; x++) {
		        for(int y = (int) pos.y; y < (int) pos.y + World.PARTSIZE; y++) {
		        	  BlockInfo block = Resources.getBlock(x, y, z);

		        	  if(block == null)
		        		  continue;

		              addBlock(block, new Vector3(x + World.PARTSIZE, y + World.PARTSIZE, z));
		        }
	        }
		}
	    pos.add(World.PARTSIZE, World.PARTSIZE);
	    this.pos = pos;
	}

	public void initEdge(int num, int direction) {
		vertices = new FloatArray();
	    indicies = new ShortArray();

	    avertices = new FloatArray();
	    aindicies = new ShortArray();

	    if(direction == TOP)
	    	pos = new Vector2(num * World.PARTSIZE - World.PARTSIZE, 0);
	    if(direction == BOTTOM)
	    	pos = new Vector2(num * World.PARTSIZE - World.PARTSIZE, 256);
	    if(direction == LEFT)
	    	pos = new Vector2(0, num * World.PARTSIZE - World.PARTSIZE);
	    if(direction == RIGHT)
	    	pos = new Vector2(256, num * World.PARTSIZE - World.PARTSIZE);

	    for(int z = 0; z < 8; z++) {
	        for(int x = (int) pos.x; x < (int) pos.x + World.PARTSIZE; x++) {
		        for(int y = (int) pos.y; y < (int) pos.y + World.PARTSIZE; y++) {
		        	if(direction == TOP) {
				        BlockInfo block = Resources.getBlock(x, 0, z);
				        if(block == null)
				        	continue;

				        addBlock(block, new Vector3(x + World.PARTSIZE, y, z));
		        	}
		        	if(direction == BOTTOM) {
				        BlockInfo block = Resources.getBlock(x, 255, z);
				        if(block == null)
				        	continue;

				        addBlock(block, new Vector3(x + World.PARTSIZE, y + World.PARTSIZE, z));
		        	}
		        	if(direction == LEFT) {
		        		int dy = y;
				        if(num == 0)
				        	dy = 0;
				        if(num == 17)
				        	dy = 255;

				        BlockInfo block = Resources.getBlock(0, dy, z);
				        if(block == null)
				        	continue;

				        addBlock(block, new Vector3(x, y + World.PARTSIZE, z));
		        	}
		        	if(direction == RIGHT) {
		        		int dy = y;
				        if(num == 0)
				        	dy = 0;
				        if(num == 17)
				        	dy = 255;

				        BlockInfo block = Resources.getBlock(255, dy, z);
				        if(block == null)
				        	continue;

				        addBlock(block, new Vector3(x + World.PARTSIZE, y + World.PARTSIZE, z));
		        	}
		        }
	        }
		}
	    if(direction == TOP)
	    	pos.add(World.PARTSIZE, 0);
	    if(direction == BOTTOM)
	    	pos.add(World.PARTSIZE, 0);
	    if(direction == LEFT)
	    	pos.add(0, World.PARTSIZE);
	    if(direction == RIGHT)
	    	pos.add(0, World.PARTSIZE);
	}

	public void addLight(Light light) {
		lights.add(light);
	}

	public void addBlock(BlockInfo block, Vector3 pos) {

		if(block.slope_type == 1)
			new Block26L(this, block, pos, Block.UP);
		else if(block.slope_type == 2)
			new Block26H(this, block, pos, Block.UP);
		else if(block.slope_type == 3)
			new Block26L(this, block, pos, Block.DOWN);
		else if(block.slope_type == 4)
			new Block26H(this, block, pos, Block.DOWN);
		else if(block.slope_type == 5)
			new Block26L(this, block, pos, Block.RIGHT);
		else if(block.slope_type == 6)
			new Block26H(this, block, pos, Block.RIGHT);
		else if(block.slope_type == 7)
			new Block26L(this, block, pos, Block.LEFT);
		else if(block.slope_type == 8)
			new Block26H(this, block, pos, Block.LEFT);
		else if(block.slope_type == 9)
			new Block7(this, block, pos, Block.UP, 1);
		else if(block.slope_type == 10)
			new Block7(this, block, pos, Block.UP, 2);
		else if(block.slope_type == 11)
			new Block7(this, block, pos, Block.UP, 3);
		else if(block.slope_type == 12)
			new Block7(this, block, pos, Block.UP, 4);
		else if(block.slope_type == 13)
			new Block7(this, block, pos, Block.UP, 5);
		else if(block.slope_type == 14)
			new Block7(this, block, pos, Block.UP, 6);
		else if(block.slope_type == 15)
			new Block7(this, block, pos, Block.UP, 7);
		else if(block.slope_type == 16)
			new Block7(this, block, pos, Block.UP, 8);
		else if(block.slope_type == 17)
			new Block7(this, block, pos, Block.DOWN, 1);
		else if(block.slope_type == 18)
			new Block7(this, block, pos, Block.DOWN, 2);
		else if(block.slope_type == 19)
			new Block7(this, block, pos, Block.DOWN, 3);
		else if(block.slope_type == 20)
			new Block7(this, block, pos, Block.DOWN, 4);
		else if(block.slope_type == 21)
			new Block7(this, block, pos, Block.DOWN, 5);
		else if(block.slope_type == 22)
			new Block7(this, block, pos, Block.DOWN, 6);
		else if(block.slope_type == 23)
			new Block7(this, block, pos, Block.DOWN, 7);
		else if(block.slope_type == 24)
			new Block7(this, block, pos, Block.DOWN, 8);
		else if(block.slope_type == 25)
			new Block7(this, block, pos, Block.RIGHT, 1);
		else if(block.slope_type == 26)
			new Block7(this, block, pos, Block.RIGHT, 2);
		else if(block.slope_type == 27)
			new Block7(this, block, pos, Block.RIGHT, 3);
		else if(block.slope_type == 28)
			new Block7(this, block, pos, Block.RIGHT, 4);
		else if(block.slope_type == 29)
			new Block7(this, block, pos, Block.RIGHT, 5);
		else if(block.slope_type == 30)
			new Block7(this, block, pos, Block.RIGHT, 6);
		else if(block.slope_type == 31)
			new Block7(this, block, pos, Block.RIGHT, 7);
		else if(block.slope_type == 32)
			new Block7(this, block, pos, Block.RIGHT, 8);
		else if(block.slope_type == 33)
			new Block7(this, block, pos, Block.LEFT, 1);
		else if(block.slope_type == 34)
			new Block7(this, block, pos, Block.LEFT, 2);
		else if(block.slope_type == 35)
			new Block7(this, block, pos, Block.LEFT, 3);
		else if(block.slope_type == 36)
			new Block7(this, block, pos, Block.LEFT, 4);
		else if(block.slope_type == 37)
			new Block7(this, block, pos, Block.LEFT, 5);
		else if(block.slope_type == 38)
			new Block7(this, block, pos, Block.LEFT, 6);
		else if(block.slope_type == 39)
			new Block7(this, block, pos, Block.LEFT, 7);
		else if(block.slope_type == 40)
			new Block7(this, block, pos, Block.LEFT, 8);
		else if(block.slope_type == 41)
			new Block45(this, block, pos, Block.UP);
		else if(block.slope_type == 42)
			new Block45(this, block, pos, Block.DOWN);
		else if(block.slope_type == 43)
			new Block45(this, block, pos, Block.RIGHT);
		else if(block.slope_type == 44)
			new Block45(this, block, pos, Block.LEFT);
		else if(block.slope_type == 45)
			new DiagonalFacingBlock(this, block, pos, 0);
		else if(block.slope_type == 46)
			new DiagonalFacingBlock(this, block, pos, 1);
		else if(block.slope_type == 47)
			new DiagonalFacingBlock(this, block, pos, 2);
		else if(block.slope_type == 48)
			new DiagonalFacingBlock(this, block, pos, 3);
		else if(block.slope_type == 49)
			new DiagonalSlopeBlock(this, block, pos, 0);
		else if(block.slope_type == 50)
			new DiagonalSlopeBlock(this, block, pos, 1);
		else if(block.slope_type == 51)
			new DiagonalSlopeBlock(this, block, pos, 2);
		else if(block.slope_type == 52)
			new DiagonalSlopeBlock(this, block, pos, 3);
		else if(block.slope_type == 53)
			new PartialBlock(this, block, pos, Block.LEFT);
		else if(block.slope_type == 54)
			new PartialBlock(this, block, pos, Block.RIGHT);
		else if(block.slope_type == 55)
			new PartialBlock(this, block, pos, Block.UP);
		else if(block.slope_type == 56)
			new PartialBlock(this, block, pos, Block.DOWN);
		else if(block.slope_type == 57)
			new PartialBlock(this, block, pos, 4);
		else if(block.slope_type == 58)
			new PartialBlock(this, block, pos, 5);
		else if(block.slope_type == 59)
			new PartialBlock(this, block, pos, 6);
		else if(block.slope_type == 60)
			new PartialBlock(this, block, pos, 7);
		else if(block.slope_type == 61)
			new PartialBlock(this, block, pos, 8);
		else
			new CubeBlock(this, block, pos);

	}

	public void buildMesh() {
        try {
        	if(vertices.size > 0) {
        		mesh = new Mesh(true,  vertices.size / 9,  indicies.size, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked());
        		mesh.setVertices(vertices.toArray());
                mesh.setIndices(indicies.toArray());

                vertices.clear();
                indicies.clear();
                vertices = null;
                indicies = null;
        	}
        	if(avertices.size > 0) {
        		amesh = new Mesh(true,  avertices.size / 9,  aindicies.size, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked());
        		amesh.setVertices(avertices.toArray());
                amesh.setIndices(aindicies.toArray());

                avertices.clear();
                aindicies.clear();
                avertices = null;
                aindicies = null;
        	}
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
	}

	public void calc_staticLights() {
		int numAttributes = 9;
		for(Light light : lights) {
			for(int vertice = 0; vertice < vertices.size / numAttributes; vertice++)
				calcVLight(vertices, vertice, light, numAttributes);
			for(int vertice = 0; vertice < avertices.size / numAttributes; vertice++)
				calcVLight(avertices, vertice, light, numAttributes);
		}
	}

	public void calcVLight(FloatArray vertices, int vertice, Light light, int numAttributes) {
		int dPos = 0;
		if(numAttributes == 13) dPos = 1;

		float x = vertices.get(vertice * numAttributes + 0 + dPos);
		float y = vertices.get(vertice * numAttributes + 1 + dPos);
		float z = vertices.get(vertice * numAttributes + 2 + dPos);

		float dx = x - light.getPosition().x;
		float dy = y - light.getPosition().z;
		float dz = z - light.getPosition().y;

		float dist = dx * dx + dy * dy + dz * dz;
		float radius = light.getLight().intensity;

		if(dist <= (radius * radius)) {
			float k_QuadricAttenuation = 0.4f;

			float attenuation = (radius - (1f/ radius) * dist) / (1 + k_QuadricAttenuation * dist);
			if(attenuation > 1) attenuation = 1;
			if(attenuation < 0) attenuation = 0;


			float r = light.getLight().color.r * attenuation;
			float g = light.getLight().color.g * attenuation;
			float b = light.getLight().color.b * attenuation;

			float[] color = {
				vertices.get(vertice * numAttributes + 5 + dPos) + r,
				vertices.get(vertice * numAttributes + 6 + dPos) + g,
				vertices.get(vertice * numAttributes + 7 + dPos) + b
			};

			vertices.set(vertice * numAttributes + 5 + dPos, color[0]);
			vertices.set(vertice * numAttributes + 6 + dPos, color[1]);
			vertices.set(vertice * numAttributes + 7 + dPos, color[2]);
		}
	}

	public boolean isVisible() {
		return !(pos.y > Renderer.viewPort[3] || (pos.y + World.PARTSIZE) < Renderer.viewPort[1] || (pos.x + World.PARTSIZE) < Renderer.viewPort[0] || pos.x > Renderer.viewPort[2]);
	}

	public Vector2 getPosition() {
		return pos;
	}

	public List<Light> getLights() {
		return lights;
	}

	public Mesh getMesh(MeshType type) {
		if(type == MeshType.Alpha)
			return amesh;
		return mesh;
	}

	public FloatArray getVertices(MeshType type) {
		if(type == MeshType.Alpha)
			return avertices;
		return vertices;
	}

	public ShortArray getIndicies(MeshType type) {
		if(type == MeshType.Alpha)
			return aindicies;
		return indicies;
	}
}