//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx;

import java.util.ArrayList;
import java.util.List;

import ru.m210projects.gta2gdx.map.Animation;
import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.GmpReader;
import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.map.Object;
import ru.m210projects.gta2gdx.map.Zone;
import ru.m210projects.gta2gdx.style.CarInfo;
import ru.m210projects.gta2gdx.style.DeltaIndex;
import ru.m210projects.gta2gdx.style.FontBase;
import ru.m210projects.gta2gdx.style.ObjectInfo;
import ru.m210projects.gta2gdx.style.Palette;
import ru.m210projects.gta2gdx.style.PaletteBase;
import ru.m210projects.gta2gdx.style.SpriteBase;
import ru.m210projects.gta2gdx.style.SpriteEntry;
import ru.m210projects.gta2gdx.style.StyReader;
import ru.m210projects.gta2gdx.tools.CarModels;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.IntArray;


public class Resources {

	//Style resources
	public static byte[] sprite_data;
	public static byte[] delta_data;
	public static short[] paletteIndex;
	public static Palette[] palettes;
	public static PaletteBase palettebase;
	public static SpriteEntry[] sprite_entries;
	public static SpriteBase spritebase;
	public static List<CarInfo> cars_infos = new ArrayList<CarInfo>();
	public static List<ObjectInfo> obj_infos = new ArrayList<ObjectInfo>();
	public static List<DeltaIndex> deltax = new ArrayList<DeltaIndex>();
	public static IntArray carrec_info = new IntArray();
	public static FontBase[] fontbases;

	//Map resources
	public static String mapname;
	public static BlockInfo[][][] city_scape;
	public static int[][] groundz;
	public static int[][] roadz;
	public static List<Object> mobj = new ArrayList<Object>();
	public static List<Animation> anim = new ArrayList<Animation>();
	public static List<Zone> zones = new ArrayList<Zone>();
	public static Texture atlas;

	public static final MapPart[] parts = new MapPart[World.MAPPARTS * World.MAPPARTS];

	private StyReader sty;
	private GmpReader gmp;


	public Resources(String styname, String mapname) {
		Resources.mapname = mapname;
		sty = new StyReader(styname + ".sty");
		paletteIndex = sty.chunk_palindex();
		palettes = sty.chunk_pals();
		atlas = sty.chunk_tiles();
		palettebase = sty.chunk_palb();
		sprite_entries = sty.chunk_sprx();
		sprite_data = sty.chunk_sprg();
		spritebase = sty.chunk_sprb();
		deltax = sty.chunk_deltax();
		delta_data = sty.chunk_deltas();
		cars_infos = sty.chunk_cars();
		CarModels.initCars();
		carrec_info = sty.chunk_reci();
		obj_infos = sty.chunk_objects();
		fontbases = sty.chunk_font();

		gmp = new GmpReader(mapname + ".gmp");
		groundz = new int[256][256];
		roadz = new int[256][256];
		city_scape = gmp.chunk_dmap();
		zones = gmp.chunk_zones();
		mobj = gmp.chunk_mobj();
		anim = gmp.chunk_anim();

		for(int i = 0; i < parts.length; i++)
			parts[i] = new MapPart(i);
		if(Config.lights)
			gmp.chunk_lght();
	}

	public static BlockInfo getBlock(int x, int y, int z) {
		return city_scape[z][x][y];
	}

	public static MapPart[] getParts() {
		return parts;
	}
}
