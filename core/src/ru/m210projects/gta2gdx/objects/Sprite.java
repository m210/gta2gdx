//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

import java.nio.ByteBuffer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

import ru.m210projects.gta2gdx.Renderer;
import ru.m210projects.gta2gdx.Resources;

public abstract class Sprite {
	public Vector3 pos;
	public Matrix4 transform;
	public int palnum;
	public float ang;
	public String name;

	public boolean onwater = false;
	public boolean onground = false;

	public Sprite(float x, float y, float z, float ang, int palnum) {
		this.ang = ang;
		this.palnum = palnum;
		transform = new Matrix4();
		pos = new Vector3(x, z, y);
	}

	public abstract void render(ShaderProgram shader);
	public abstract void update(float delta);

	public boolean isVisible() {
		return !(pos.z - 0.5f > Renderer.viewPort[3] || (pos.z + 0.5f) < Renderer.viewPort[1] || (pos.x + 0.5f) < Renderer.viewPort[0] || pos.x - 0.5f > Renderer.viewPort[2]);
	}

	public static ByteBuffer getData(int spriteid) {
		int sprw = Resources.sprite_entries[spriteid].w;
		int sprh = Resources.sprite_entries[spriteid].h;

		int base  = Resources.sprite_entries[spriteid].ptr;

		ByteBuffer buf = ByteBuffer.allocateDirect(sprw*sprh);
		byte pixel[] = new byte[sprw];
		for (int y = 0; y < sprh; y++) {
			for (int x = 0; x < sprw; x++) {
				pixel[x] = Resources.sprite_data[base + x + (y << 8)];
			}
			buf.put(pixel);
		}
		pixel = null;
		return buf;
	}

	public static Texture getTexture(int spriteid) {
		int sprw = Resources.sprite_entries[spriteid].w;
		int sprh = Resources.sprite_entries[spriteid].h;

		int base  = Resources.sprite_entries[spriteid].ptr;

		ByteBuffer buf = ByteBuffer.allocateDirect(sprw*sprh);
		byte pixel[] = new byte[sprw];
		for (int y = 0; y < sprh; y++) {
			for (int x = 0; x < sprw; x++) {
				pixel[sprw - x - 1] = Resources.sprite_data[base + x + (y << 8)]; //y * 256
			}
			buf.put(pixel);
		}
		buf.flip();

		Texture tex = new Texture(sprw, sprh, Format.RGB565);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
		//Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_LINEAR);
		//Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MIN_FILTER, GL20.GL_LINEAR);
		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_LUMINANCE, sprw, sprh, 0, GL20.GL_LUMINANCE, GL20.GL_UNSIGNED_BYTE, buf);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);

		pixel = null;
		buf.clear();
		return tex;
	}
}
