//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.map.BlockInfo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Player extends Ped {
	//debug
	boolean go = false;

	public Player(float x, float y, float z, int rotation, int modelnum, int palette) {
		super(x, y, z, rotation, modelnum, palette);
		name = "Player";
	}
	@Override
	public void movingControl(float delta) {
		debug();

		double angle = Math.toRadians(ang);
		direction.set((float) Math.cos(angle), (float) Math.sin(angle));

		if(Gdx.input.isKeyPressed(Keys.LEFT))
        	ang += 150 * delta % 360;

        if(Gdx.input.isKeyPressed(Keys.RIGHT))
        	ang -= 150 * delta;

        if(Gdx.input.isKeyPressed(Keys.UP)) {
        	speed = PEDSPEED;
        } else if(Gdx.input.isKeyPressed(Keys.DOWN)) {
        	speed = -PEDSPEED;
        } else speed = 0;

        if(go || Gdx.input.isTouched()) {
        	float tx = (float) (Gdx.input.getX() / (Gdx.graphics.getWidth() / 320.0));
        	float ty = (float) (Gdx.input.getY() / (Gdx.graphics.getHeight() / 240.0));

			direction.set(tx - 160, 120 - ty);
			direction.nor();
			speed = PEDSPEED;

			ang = (int) (direction.angle());
        }

        if(Gdx.input.isKeyJustPressed(Keys.SPACE))
        	jumping = true;

        if(Gdx.input.isKeyPressed(Keys.CONTROL_LEFT))
			shooting = true;

        if(Gdx.input.isKeyJustPressed(Keys.Z))  {
        	if(currweapon > 0) currweapon--;
        }
        if(Gdx.input.isKeyJustPressed(Keys.X))  {
        	if(currweapon < 12) currweapon++;
        }
	}

	public void debug() {
		if(Gdx.input.isKeyJustPressed(Keys.G)) {
	     	go = !go;
	    }
		if(Gdx.input.isKeyJustPressed(Keys.PLUS)) {
			//pos.y += 1f;
		}
		if(Gdx.input.isKeyJustPressed(Keys.MINUS)) {
			//pos.y -= 1f;
		}
		if(Gdx.input.isKeyJustPressed(Keys.P)) {
			System.out.println("Position " + "(" + (pos.x - World.PARTSIZE) + ", " + pos.y + ", "+ (pos.z - World.PARTSIZE) + ")");

			int x = (int) (pos.x / World.PARTSIZE);
			int y = (int) (pos.z / World.PARTSIZE);
			int currentChunk = World.MAPPARTS * x + y;

			System.out.println("currentChunk " + currentChunk);

			BlockInfo curr_block = Resources.getBlock((int) pos.x - World.PARTSIZE, (int) pos.z - World.PARTSIZE, (int) pos.y);
			System.out.println("Block " + curr_block);
			if(curr_block != null) {
				System.out.println("Block arrows " + curr_block.arrows);
				System.out.println("Block slope type " + curr_block.slope_type);
				System.out.println("Block ground type " + curr_block.ground_type);
				System.out.println("Block lid texture " + curr_block.lid);
				System.out.println("Block top texture " + curr_block.top);
				System.out.println("Block bottom texture " + curr_block.bottom);
				System.out.println("Block left texture " + curr_block.left);
				System.out.println("Block right texture " + curr_block.right);

			}
			System.out.println("Road Z " + World.getRoad(pos.x, pos.z));
			System.out.println("Ground Z " + World.getGround(pos.x, pos.z));
		}
	}
}
