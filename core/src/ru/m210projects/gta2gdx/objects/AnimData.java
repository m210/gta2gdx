//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

public class AnimData {
	public final int startFrame;
	public final int length;
	final float frameDuration;
	int frameNumber;
	float stateTime = 0;
	int actioncount = 0;


	public AnimData(int startFrame, int length, float frameDuration) {
		this.startFrame = startFrame;
		this.length = length;
		this.frameDuration = frameDuration;
	}

	public int getKeyFrame(float delta) {
        stateTime += delta;
        frameNumber = (int)(stateTime / frameDuration);
        actioncount = frameNumber;
        frameNumber = startFrame + frameNumber % length;

        return frameNumber;
    }

	public float getTotalTime() {
		return length * frameDuration;
	}

	public int getFrame() {
		return frameNumber - startFrame;
	}

	public void resetactioncount() {
		stateTime = 0;
		actioncount = 0;
	}

	public boolean isStarted() {
		return stateTime > 0;
	}

	public boolean isEnd() {
		return Math.abs(actioncount) >= length;
	}
}
