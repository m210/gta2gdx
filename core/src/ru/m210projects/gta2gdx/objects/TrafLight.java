//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;


import java.nio.ByteBuffer;

import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.style.PaletteBase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class TrafLight extends Sprite {
	private static Mesh mesh;
	private static Texture[] tex;

	private int spriteid;
	private int color = 0;

	public TrafLight(float x, float y, float z, int ang) {
		super(x, y, z, ang, 0);

		spriteid = 1099;
		palnum = Resources.paletteIndex[PaletteBase.SPRITE + spriteid];
		if(tex == null)  {
			tex = new Texture[3];
			for(int i = 0; i < 3; i++)
				tex[i] = buildLights(i);
		}

		if(mesh == null) {
			float SIZEX = Resources.sprite_entries[spriteid].w / 128f;
			float SIZEY = Resources.sprite_entries[spriteid].h / 128f;

			float[] vertices = {
				-SIZEX,  0.1f,	SIZEY, 	1, 0, 1,1,1,1,
				SIZEX, 	 0.1f,	SIZEY, 	1, 1, 1,1,1,1,
			    SIZEX, 	 0.1f, 	-SIZEY, 0, 1, 1,1,1,1,
			    -SIZEX,  0.1f,  -SIZEY, 0, 0, 1,1,1,1,
			};
			short[] indicies = { 0, 1, 2, 2, 3, 0 };

			mesh = new Mesh(false,  vertices.length / 9,  indicies.length, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked() );
			mesh.setVertices(vertices);
	        mesh.setIndices(indicies);
	        vertices = null;
	        indicies = null;
		}
	}

	public void setColor(int color) {
		this.color = color;
	}

	@Override
	public void render(ShaderProgram shader) {
		tex[color].bind(0);
		transform.idt();
		transform.translate(pos);
		transform.rotate(0, 1, 0, ang);

		Resources.palettes[palnum].getTexture().bind(1);
		shader.setUniformi("u_pal", 1);
		shader.setUniformMatrix("u_worldTrans", transform);
		shader.setUniformi("u_shadow", 0);
		mesh.render(shader, GL20.GL_TRIANGLES);
	}

	@Override
	public void update(float delta) {
		int dir = World.traf_mng.trafficDirection;
		if(dir == 0) {
			if(ang == 0 || ang == 180)
				setColor(0);
			else if(ang == 90 || ang == 270)
				setColor(1);
		} else if(dir == 1) {
			if(ang == 0 || ang == 180)
				setColor(1);
			else if(ang == 90 || ang == 270)
				setColor(0);
		} else setColor(dir);
	}

//	public Texture buildLights() {
//		int sprw = Resources.sprite_entries[spriteid].w;
//		int sprh = Resources.sprite_entries[spriteid].h;
//
//		int base  = Resources.sprite_entries[spriteid].ptr;
//
//		ByteBuffer buf = ByteBuffer.allocateDirect(3*sprw*sprh);
//		byte pixel[] = new byte[sprw];
//		for (int y = 0; y < sprh; y++) {
//			for (int x = 0; x < sprw; x++)
//				pixel[x] = Resources.sprite_data[base + x + y * 256];
//
//			for (int t = 0; t < 3; t++) {
//				int dw = Resources.sprite_entries[spriteid + t + 1].w;
//				int dh = Resources.sprite_entries[spriteid + t + 1].h;
//				int yoffset = 15;
//				if(y >= yoffset && (y - yoffset) < dh) {
//					int dy = y - yoffset;
//					int dbase  = Resources.sprite_entries[spriteid + t + 1].ptr;
//
//					for (int x = 0; x < dw; x++)
//						pixel[x + 2] = Resources.sprite_data[dbase + x + (dy << 8)];
//				}
//				buf.put(pixel);
//			}
//		}
//		buf.rewind();
//
//
//		Texture tex = new Texture(3*sprw, sprh, Format.RGB565);
//		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
//		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_LUMINANCE, 3*sprw, sprh, 0, GL20.GL_LUMINANCE, GL20.GL_UNSIGNED_BYTE, buf);
//		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
//		return tex;
//	}


	public Texture buildLights(int color) {
		int sprw = Resources.sprite_entries[spriteid].w;
		int sprh = Resources.sprite_entries[spriteid].h;

		int base  = Resources.sprite_entries[spriteid].ptr;

		ByteBuffer buf = ByteBuffer.allocateDirect(sprw*sprh);
		byte pixel[] = new byte[sprw];
		for (int y = 0; y < sprh; y++) {
			for (int x = 0; x < sprw; x++)
				pixel[x] = Resources.sprite_data[base + x + y * 256];


			int dw = Resources.sprite_entries[spriteid + color + 1].w;
			int dh = Resources.sprite_entries[spriteid + color + 1].h;
			int yoffset = 15;
			if(y >= yoffset && (y - yoffset) < dh) {
				int dy = y - yoffset;
				int dbase  = Resources.sprite_entries[spriteid + color + 1].ptr;

				for (int x = 0; x < dw; x++)
					pixel[x + 2] = Resources.sprite_data[dbase + x + (dy << 8)];
			}
			buf.put(pixel);

		}
		buf.rewind();


		Texture tex = new Texture(sprw, sprh, Format.RGB565);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_LUMINANCE, sprw, sprh, 0, GL20.GL_LUMINANCE, GL20.GL_UNSIGNED_BYTE, buf);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
		return tex;
	}

}


