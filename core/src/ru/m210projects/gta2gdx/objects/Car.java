//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

import java.nio.ByteBuffer;

import ru.m210projects.gta2gdx.Config;
import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.style.DeltaIndex;
import ru.m210projects.gta2gdx.style.PaletteBase;
import ru.m210projects.gta2gdx.tools.CarModels;
import ru.m210projects.gta2gdx.tools.LittleEndian;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


public  class Car extends Sprite {

	private int trailerid = -1;
	private Mesh mesh;
	private Texture tex;
	private final static ByteBuffer bases[] = new ByteBuffer[CarModels.NUMMODELS];
	private ByteBuffer base;

	private int spriteid;

	private int model;

	private float rear_wheel_offset;
	private float front_wheel_offset;
	float wheelBase;

	public int sprw;
	public int sprh;

	protected float speed;
	protected Vector2 velocity;
	protected Vector2 direction;
	private boolean isParked = false;
	private Vector3 scale;

	public Car() {
		super(0, 0, 0, 0, 0);
		isParked = false;

		name = "Car";

		buildMesh();

	    velocity = new Vector2();
		direction = new Vector2();
	}

	public Car(float x, float y, float z, int ang, int model, int palette, boolean isParked) {
		super(x, y, z, ang, palette);
		this.model = model;
		this.isParked = isParked;
		name = "Car";

		changeModel(model);
		changePal(palette);

		buildMesh();

	    velocity = new Vector2();
		direction = new Vector2();
	}

	public void buildMesh() {
		float SIZEX = 0.5f;
		float SIZEY = 0.5f;

		float[] vertices = {
			-SIZEY,  0.1f,	SIZEX, 	1, 0, 1,1,1,1,
			SIZEY, 	 0.1f,	SIZEX, 	1, 1, 1,1,1,1,
			SIZEY, 	 0.1f, 	-SIZEX, 0, 1, 1,1,1,1,
			-SIZEY,  0.1f,  -SIZEX, 0, 0, 1,1,1,1,
		};
		short[] indicies = { 0, 1, 2, 2, 3, 0 };

		mesh = new Mesh(true,  vertices.length / 9,  indicies.length, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked() );
		mesh.setVertices(vertices);
	    mesh.setIndices(indicies);
	    vertices = null;
	    indicies = null;
	}

	public void changeModel(int model) {
		this.model = model;
		spriteid = CarModels.sprite_array[model];
		int index = CarModels.model_index[model];

		sprw = Resources.sprite_entries[spriteid].w;
		sprh = Resources.sprite_entries[spriteid].h;

		rear_wheel_offset = Resources.cars_infos.get(index).rear_wheel_offset / 48.0f;
		front_wheel_offset = Resources.cars_infos.get(index).front_wheel_offset / 48.0f;
		wheelBase = front_wheel_offset - rear_wheel_offset;

		resetBase(model);
		if(!isParked) {
			putDelta(base, 5, false);
			putDelta(base, 5, true);
			putDelta(base, 6, false);
			putDelta(base, 6, true);
		}
		tex = buildTex(base);
		if(scale == null)
			scale = new Vector3();
		scale.x = sprh / 64.0f;
		scale.z = sprw / 64.0f;
	}

	@Override
	public void render(ShaderProgram shader) {
		tex.bind(0);

		transform.idt();
		transform.translate(pos);
		transform.rotate(0, 1, 0, ang);
		transform.scale(scale.x, 1, scale.z);
		shader.setUniformMatrix("u_worldTrans", transform);

		Resources.palettes[palnum].getTexture().bind(1);
		shader.setUniformi("u_pal", 1);

		shader.setUniformi("u_shadow", 0);
		mesh.render(shader, GL20.GL_TRIANGLES);
		if(Config.shadows) {
			shader.setUniformi("u_shadow", 1);
			mesh.render(shader, GL20.GL_TRIANGLES);
		}
	}

	public void setParked(boolean isParked) {
		this.isParked = isParked;
	}

	public void setTrailer(int trailerid) {
		this.trailerid = trailerid;
	}

	public void changePal(int palette) {
		int remap = 0;
		int index = CarModels.model_index[model];

		if(palette > 0 && Resources.cars_infos.get(index).remap != null && palette <= Resources.cars_infos.get(index).remap.length) {
			remap = Resources.cars_infos.get(index).remap[palette - 1] + 1;
		}

		if(remap > 0 && remap <= PaletteBase.CAR) {
			palnum = Resources.paletteIndex[PaletteBase.CAR + remap - 1];
		} else palnum = Resources.paletteIndex[PaletteBase.SPRITE + spriteid];
	}

	public void action() {
		if(Gdx.input.isKeyPressed(Keys.NUM_1)) {
			resetBase(model);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_2)) {

			putDelta(base, 0, false);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_3)) {

			putDelta(base, 1, false);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_4)) {

			putDelta(base, 2, false);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_5)) {

			putDelta(base, 3, false);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_6)) {

			putDelta(base, 4, false);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_7)) {

			putDelta(base, 5, false);
			putDelta(base, 5, true);
			putDelta(base, 6, false);
			putDelta(base, 6, true);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_8)) {

			putDelta(base, 7, false);
			putDelta(base, 7, true);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_9)) {

			putDelta(base, 8, false);
			putDelta(base, 8, true);
			tex = buildTex(base);
		}
		if(Gdx.input.isKeyPressed(Keys.NUM_0)) {

			putDelta(base, 9, false);
			putDelta(base, 9, true);
			tex = buildTex(base);
		}
	}

	@Override
	public void update(float delta) {

		if(trailerid == -1 && !isParked) {

			speed = 2f;

			if(World.getRoad(pos.x, pos.z) == -1 || World.road_blocked[(int)(pos.x - World.PARTSIZE + direction.x)][(int)(pos.z - World.PARTSIZE - direction.y)])
				speed = 0;
			else {
				int x = (int)(pos.x - World.PARTSIZE);
				int y = (int)(pos.z - World.PARTSIZE);
				int z = (int) pos.y - 1;

				BlockInfo roadBlock = Resources.getBlock(x, y, z);
				if(roadBlock != null) {

					int carDirection = 2;
					int roadDirection = -1;

					if(ang == 90)
						carDirection = 4;
					else if(ang == 180)
						carDirection = 1;
					else if(ang == 270)
						carDirection = 8;

					if((carDirection & roadBlock.arrows) == carDirection) {
						/*
						if(World.rnd.nextBoolean()) {
							int quad = World.rnd.nextInt(4);
							int direction = 1 << quad;

							if((roadBlock.arrows & direction) == 1)
								roadDirection = 180;
							if((roadBlock.arrows & direction) == 2)
								roadDirection = 0;
							if((roadBlock.arrows & direction) == 4)
								roadDirection = 90;
							if((roadBlock.arrows & direction) == 8)
								roadDirection = 270;

							if(roadDirection == -1)
								roadDirection = (int) ang;
						} else
						*/
						roadDirection = (int) ang;
					} else {
						if((roadBlock.arrows & 1) != 0)
							roadDirection = 180;
						if((roadBlock.arrows & 2) != 0)
							roadDirection = 0;
						if((roadBlock.arrows & 4) != 0)
							roadDirection = 90;
						if((roadBlock.arrows & 8) != 0)
							roadDirection = 270;
					}

					if(ang != roadDirection) {
						ang = roadDirection;
						pos.x = x + 0.5f + World.PARTSIZE;
						pos.z = y + 0.5f + World.PARTSIZE;
					}
				} else {
					speed = 0;
					System.out.println("Stupid car at " + (x + World.PARTSIZE) + " : " + (y + World.PARTSIZE));
				}
			}

			double angle = Math.toRadians(ang);
			direction.set((float) Math.cos(angle), (float) Math.sin(angle));


			velocity.x = direction.x * speed * delta;
	        velocity.y = direction.y * speed * delta;

	        pos.x += velocity.x;
	        pos.z -= velocity.y;
		}
	}

	public Texture buildTex(ByteBuffer buf) {
		buf.rewind();

		if(tex == null)
			tex = new Texture(sprw, sprh, Format.RGB565);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_LUMINANCE, sprw, sprh, 0, GL20.GL_LUMINANCE, GL20.GL_UNSIGNED_BYTE, buf);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);

		return tex;
	}

	public void putDelta(ByteBuffer buf, int deltanum, boolean mirror) {
		int offset = 0;

		int sprw = Resources.sprite_entries[spriteid].w;

		byte[] data = Resources.delta_data;
		for (DeltaIndex deltaIndex : Resources.deltax) {
			if (deltaIndex.which_sprite == spriteid) {
				if (deltaIndex.size.size > 0) {
					offset = deltaIndex.offset;

					for (int i = 0; i < deltanum; i++)
	                	offset += deltaIndex.size.get(i);

					int pos = 0;
					int pixline_len = 0;
					int deltasize = offset + deltaIndex.size.get(deltanum);
					if(!mirror) {
						while (offset < deltasize) {
							int pixline_pos = LittleEndian.getUShort(data, offset);
							offset += 2;
							pos += (pixline_pos + pixline_len);
							pixline_len = data[offset++];

		                  	int x = pos % 256;
		                    int y = pos / 256;

		                    buf.position(x + y * sprw);
		                    buf.put(data, offset, pixline_len);
		                    offset += pixline_len;
						}
					} else {
						while (offset < deltasize) {
							int pixline_pos = LittleEndian.getUShort(data, offset);
							offset += 2;
							pos += (pixline_pos + pixline_len);
							pixline_len = data[offset++];

		                  	int x = pos % 256;
		                    int y = pos / 256;

		                    for(int i = 0; i < pixline_len; i++) {
		                    	buf.position((sprw - x - 1) + y * sprw - i);
		                    	buf.put(data[offset++]);
		                    }
						}
					}

				}
			}
		}
	}

	public boolean isParked() {
		return isParked;
	}

	public Vector2 getDirection() {
		return direction;
	}

	public void resetBase(int model) {
		if(bases[model] == null)
			bases[model] = getData(CarModels.sprite_array[model]).asReadOnlyBuffer();
		if(base == null)
			base = ByteBuffer.allocateDirect(bases[model].capacity());

		//if changing model
		if(base.capacity() < bases[model].capacity()) {
			base = ByteBuffer.allocateDirect(bases[model].capacity());
		}

		bases[model].flip();
	    base.put(bases[model]);
	}
}
