//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

import java.util.ArrayList;
import java.util.Random;

import ru.m210projects.gta2gdx.Config;
import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.World;
import ru.m210projects.gta2gdx.style.PaletteBase;
import ru.m210projects.gta2gdx.style.SpriteBase;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


public class Ped extends Sprite {

	private static Mesh mesh;
	private static ArrayList<Texture> animModel1;
	private static ArrayList<Texture> animModel2;
	private static ArrayList<Texture> animModel3;
	private static ArrayList<Texture> effects;
	private int modelnum;
	private int spriteid;
	private Vector3 scale;
	private float SIZEX;
	private float SIZEY;
	public final static float RADIUS = 0.15f;

	public static float PEDSPEED = 1.75f;

	protected float speed = 0;
	protected Vector2 velocity;
	protected Vector2 direction;

	protected static final int aStay = 0;
	public static final int aWalk = 1;
	public static final int aRun = 2;
	protected static final int aJump = 3;
	protected static final int aGrab = 4;
	protected static final int aOutcar = 5;
	public static final int aWent_weap = 6;
	public static final int aRun_weap = 7;
	protected static final int aSmok = 8;
	protected static final int aFall = 9;
	protected static final int aFall2 = 10;
	protected static final int aFallDown = 11;
	protected static final int aFallDown2 = 12;
	protected static final int aFallDown3 = 13;
	protected static final int aFallDown4 = 14;
	protected static final int aFall3 = 15;
	protected static final int aStay_attack = 16;
	protected static final int aRun_attack = 17;
	protected static final int aWent_attack = 18;
	protected static final int aShoot = 19;
	protected static final int aWater = 20;
	protected static final int aElectr = 21;
	protected static final int aElectrDead = 22;
	protected static final int aDead = 23;
	protected static final int aDead2 = 24;
	protected static final int aFire = 25;
	protected static final int aIncar = 26;

	protected final AnimData[] adata = {
			new AnimData(53, 1, 0.1f), //aStay
			new AnimData(0, 8, 0.1f), //aWalk
			new AnimData(8, 8, 0.1f), //aRun
			new AnimData(16, 8, 0.1f), //aJump
			new AnimData(24, 8, 0.1f), //aGrab
			new AnimData(32, 5, 0.1f), //aOutcar
			new AnimData(37, 8, 0.1f), //aWent_weap
			new AnimData(45, 8, 0.1f), //aRun_weap
			new AnimData(53, 12, 0.1f), //aSmok
			new AnimData(65, 8, 0.1f), //aFall
			new AnimData(73, 8, 0.1f), //aFall2
			new AnimData(81, 17, 0.1f), //aFallDown
			new AnimData(97, 1, 0.1f), //aFallDown2
			new AnimData(98, 5, 0.1f), //aFallDown3
			new AnimData(103, 4, 0.1f), //aFallDown4
			new AnimData(107, 8, 0.1f), //aFall3
			new AnimData(115, 8, 0.1f), //aStay_attack
			new AnimData(123, 8, 0.1f), //aRun_attack
			new AnimData(131, 8, 0.1f), //aWent_attack
			new AnimData(139, 1, 0.1f), //aShoot
			new AnimData(143, 8, 0.1f), //aWater
			new AnimData(152, 4, 0.1f), //aElectr
			new AnimData(155, 1, 0.1f), //aElectrDead
			new AnimData(156, 1, 0.1f), //aDead
			new AnimData(157, 1, 0.1f), //aDead2
			new AnimData(0, 3, 0.02f), //aFire
			new AnimData(36, 5, -0.1f), //aIncar
	};

	protected Random rnd;

	//STATES

	protected int anim_id;
	protected int curr_frame = 0;
	protected float stayTime = 0;
	protected boolean jumping = false;
	protected boolean shooting = false;
	protected boolean smoking = false;
	protected boolean falling = false;
	protected boolean dead = false;
	protected boolean electr = false;

	protected boolean getcar = false;
	protected boolean aiGrab = false;
	protected boolean aiIncar = false;

	// WEAPONS

	protected float shootTime = 0;
	protected int currweapon = 0;
	protected static float[] totaltime = new float[12];
	protected static float[] firetime = new float[12];
	protected static boolean[] spawns = new boolean[12];
	protected static boolean[] visible = new boolean[12];
	protected static int[] shoots = new int[12];
	protected boolean shooted = false;

	protected final int firepalnum = 103;
	protected int fireframe = 0;
	protected boolean drawFire = false;

	public Ped(float x, float y, float z, int ang, int modelnum, int palette) {
		super(x, y, z, ang, palette);
		this.modelnum = modelnum;
		name = "Ped";

		spriteid = SpriteBase.PED + modelnum * 158;
		if(spriteid >= SpriteBase.CODE_OBJ) spriteid = SpriteBase.CODE_OBJ - 1;

		if(palette >= 0 && palette <= Resources.palettebase.ped_remap)
			palnum = Resources.paletteIndex[PaletteBase.PED + palette];
		//else palnum = Resources.paletteIndex[PaletteBase.SPRITE + spriteid];

		if(modelnum == 0 && animModel1 == null) {
			animModel1 = new ArrayList<Texture>(158);
			for(int i = 0; i < 158; i++)
				animModel1.add(getTexture(spriteid + i));
		}
		if(modelnum == 1 && animModel2 == null) {
			animModel2 = new ArrayList<Texture>(158);
			for(int i = 0; i < 158; i++)
				animModel2.add(getTexture(spriteid + i));
		}
		if(modelnum == 2 && animModel3 == null) {
			animModel3 = new ArrayList<Texture>(158);
			for(int i = 0; i < 158; i++)
				animModel3.add(getTexture(spriteid + i));
		}
		if(effects == null) {
			effects = new ArrayList<Texture>();
			for(int i = 0; i < 3; i++) {
				Texture tex = getTexture(1054 + i);
				effects.add(tex);
			}
		}

		if(mesh == null) {
			SIZEX = 0.5f;
			SIZEY = 0.5f;

			float[] vertices = {
				-SIZEY,  0.1f,	SIZEX, 	1, 0, 1,1,1,1,
				SIZEY, 	 0.1f,	SIZEX, 	1, 1, 1,1,1,1,
			    SIZEY, 	 0.1f, 	-SIZEX, 0, 1, 1,1,1,1,
			    -SIZEY,  0.1f,  -SIZEX, 0, 0, 1,1,1,1,
			};
			short[] indicies = { 0, 1, 2, 2, 3, 0 };

			mesh = new Mesh(true,  vertices.length / 9,  indicies.length, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked() );
			mesh.setVertices(vertices);
	        mesh.setIndices(indicies);
	        vertices = null;
	        indicies = null;
		}
		scale = new Vector3(1, 1, 1);
		velocity = new Vector2();
		direction = new Vector2();
		rnd = new Random();
		anim_id = aStay;

		init_weapons();
	}

	public void init_weapons() {
		totaltime[0] = 0.8f;
		firetime[0] = 0.1f;
		spawns[0] = false;
		visible[0] = false;

		totaltime[1] = 0.05f;
		firetime[1] = 0.02f;
		spawns[1] = true;
		visible[1] = true;
	}

	public void movingControl(float delta) {
		speed = 0.5f;

		if(World.getGround(pos.x, pos.z) == -1) {
			ang += 180;
			ang %= 360;
		}



		double angle = Math.toRadians(ang);
		direction.set((float) Math.cos(angle), (float) Math.sin(angle));
	}

	public void animState(float delta) {
		if(!dead) {
			if(speed == 0) {
				stayTime += delta;
				if(shooting) {
					if(visible[currweapon]) {
						anim_id = aShoot;
					} else {
						anim_id = aStay_attack;
					}
				}
				else {
					if(smoking) {
						if(anim_id != aSmok)
							adata[aSmok].resetactioncount();
						anim_id = aSmok;
					}
					else
						anim_id = aStay;
				}
			} else {
				stayTime = 0;
				smoking = false;
				if(!jumping) {
					if(Math.abs(speed) <= 1) {
						if(shooting) {
							if(!visible[currweapon]) {
								anim_id = aWent_attack;
							} else anim_id = aWent_weap;
						} else {
							if(visible[currweapon])
								anim_id = aWent_weap;
							else
								anim_id = aWalk;
						}
					}
					else {
						if(shooting) {
							if(!visible[currweapon]) {
								anim_id = aRun_attack;
							} else anim_id = aRun_weap;
						} else {
							if(visible[currweapon])
								anim_id = aRun_weap;
							else
								anim_id = aRun;
						}
					}
				}
			}

			if(jumping) {
		     	speed = 2;
		        anim_id = aJump;
		        if(adata[anim_id].isEnd()) {
		        	jumping = false;
		        	adata[anim_id].resetactioncount();
		        }
		    }

			if(stayTime >= 20) {
				smoking = true;
				stayTime = 0;
			}

			if(falling) {
				anim_id = aFallDown;
				if(adata[anim_id].isEnd()) {
					anim_id = aFallDown2;
				}

				if(anim_id == aFallDown2) {
					if(onground) {
						if(rnd.nextBoolean())
							anim_id = aDead;
						else anim_id = aDead2;

						falling = false;
						dead = true;
					}
				}
			}


			if(getcar) {
				aiGrab = true;
				getcar = false;
			}

			if(aiGrab) {
				anim_id = aGrab;

				if(adata[aGrab].isEnd()) {
					anim_id = aIncar;
					aiGrab = false;
					aiIncar = true;
				}
			}

			if(aiIncar) {
				anim_id = aIncar;
				if(adata[aIncar].isEnd()) {
					adata[aGrab].resetactioncount();
					adata[aIncar].resetactioncount();
					aiIncar = false;
				}
			}

			if(onwater) {
				anim_id = aWater;
				if(adata[anim_id].isEnd()) {
					dead = true;
				}
			}

			if(electr) {
				anim_id = aElectr;
				dead = true;
			}
		} else { //if dead
			if(electr) {
				if(adata[anim_id].actioncount >= 10)
					anim_id = aElectrDead;
			}
		}

		if(!visible[currweapon] && shooting) //������������� �������� ��� ����
			adata[anim_id].stateTime = shootTime;
		curr_frame = adata[anim_id].getKeyFrame(delta);
		if(drawFire) {
			fireframe = adata[aFire].getKeyFrame(delta);
			if(adata[aFire].isEnd()) {
				adata[aFire].resetactioncount();
				drawFire = false;
			}
		}
	}

	@Override
	public void render(ShaderProgram shader) {
		Texture tex;

		if(modelnum == 0)
			tex = animModel1.get(curr_frame);
		else if(modelnum == 1)
			tex = animModel2.get(curr_frame);
		else tex = animModel3.get(curr_frame);

		tex.bind(0);

		scale.x = tex.getHeight() / 64f;
		scale.z = tex.getWidth() / 64f;

		transform.idt();
		transform.translate(pos);
		transform.rotate(0, 1, 0, ang);
		transform.scale(scale.x, 1, scale.z);

		if(anim_id == aElectr) Resources.palettes[Resources.paletteIndex[PaletteBase.PED]].getTexture().bind(1); else
			Resources.palettes[palnum].getTexture().bind(1);

		shader.setUniformi("u_pal", 1);
		shader.setUniformMatrix("u_worldTrans", transform);
		shader.setUniformi("u_shadow", 0);


		mesh.render(shader, GL20.GL_TRIANGLES);
		if(Config.shadows) {
			shader.setUniformi("u_shadow", 1);
			mesh.render(shader, GL20.GL_TRIANGLES);
		}

		if(drawFire)
			drawFire(shader);
	}

	public void drawFire(ShaderProgram shader) {
		Texture tex = effects.get(fireframe);
		tex.bind(0);

		scale.x = tex.getHeight() / 64f;
		scale.z = tex.getWidth() / 64f;

		float sin = (float) Math.sin(Math.toRadians(ang));
		float cos = (float) Math.cos(Math.toRadians(ang));
		float radius = 0.29f;
		if(anim_id == Ped.aRun_weap)
			radius = 0.34f;

		float x = pos.x + radius * cos + 0.05f * sin;
		float y = pos.z - radius * sin + 0.05f * cos;
		float z = pos.y;

		transform.idt();
		transform.translate(x, z, y);
		transform.rotate(0, 1, 0, ang);
		transform.scale(scale.x, 1, scale.z);

		Resources.palettes[firepalnum].getTexture().bind(1);
		shader.setUniformi("u_pal", 1);
		shader.setUniformMatrix("u_worldTrans", transform);
		shader.setUniformi("u_shadow", 0);
		shader.setUniformf("u_transparent", 0.5f);
		mesh.render(shader, GL20.GL_TRIANGLES);
		shader.setUniformf("u_transparent", 0.0f);
	}

	public void weapons(float delta) {
		if(shooting) {
			shootTime += delta;

			if(shootTime >= firetime[currweapon] && !shooted) {
				if(spawns[currweapon])
					drawFire = true;
				shooted = true;
			}

			if(shootTime >= totaltime[currweapon]) {
				shooting = false;
				shooted = false;
	        	shootTime = 0;
			}
	    }
	}

	public float getSpeed() {
		return speed;
	}

	public Vector2 getDirection() {
		return direction;
	}

	@Override
	public void update(float delta) {
		if(ang < 0) ang += 360;
		ang %= 360;

		if(!dead)
			movingControl(delta); //speed + ang

		animState(delta);

		weapons(delta);

		velocity.x = direction.x * speed * delta;
        velocity.y = direction.y * speed * delta;

        pos.x += velocity.x;
        pos.z -= velocity.y;

        //pos.y -= World.GRAVITY * delta;
	}
}
