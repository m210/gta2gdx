//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.style.PaletteBase;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class SDebug extends Sprite {
	private static Mesh mesh;
	private static Texture tex;

	private int spriteid;

	public SDebug(float x, float y, float z) {
		super(x, y, z, 0, 0);
		spriteid = 1099;
		palnum = Resources.paletteIndex[PaletteBase.SPRITE + spriteid];
		if(tex == null)  {
			tex = getTexture(spriteid);
		}

		if(mesh == null) {
			float[] vertices = {
				0,   0.1f,	1, 	1, 0, 0,0,0,1,
				1, 	 0.1f,	1, 	1, 1, 0,0,0,1,
			    1, 	 0.1f, 	0, 0, 1,  0,0,0,1,
			    0,   0.1f,   0, 0, 0,   0,0,0,1
			};
			short[] indicies = { 0, 1, 2, 2, 3, 0 };

			mesh = new Mesh(false,  vertices.length / 9,  indicies.length, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked() );
			mesh.setVertices(vertices);
	        mesh.setIndices(indicies);
	        vertices = null;
	        indicies = null;
		}
	}

	@Override
	public void render(ShaderProgram shader) {
		tex.bind(0);
		transform.idt();
		transform.translate(pos);

		shader.setUniformMatrix("u_worldTrans", transform);
		shader.setUniformi("u_shadow", 0);
		mesh.render(shader, GL20.GL_TRIANGLES);
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub

	}

}
