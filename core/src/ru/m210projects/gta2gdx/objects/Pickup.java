//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.objects;

import ru.m210projects.gta2gdx.Config;
import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.style.PaletteBase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

public class Pickup extends Sprite {
	private static Mesh mesh;

	private int spriteid;
	private Vector2 scale;
	private static final Texture data[][] = new Texture[45][];
	private Texture[] frames;
	private int frame;
	private float count = 0;
	private int anim_len = 0;
	private boolean hasShadows = true;


	public static final String[] name = {
		"Pistol",
		"Machinegun",
		"RocketLauncher",
		"Electrogun",
		"Molotov",
		"Granades",
		"Shotgun",
		"ElectroBaton",
		"Flamethrower",
		"Silenced Machinegun",
		"DualPistol",
		"YellowL",
		"YellowM",
		"YellowN",
		"YellowO",
		"CarTNT",
		"CarOil",
		"CarMines",
		"CarGuns",

		"CarRocketL",
		"CarWaterCannon",
		"CarFlameThrower",
		"LandRoamerMachinegun",
		"InstantBomb",
		"BlueJ",
		"BlueK",
		"BlueL",
		"BlueM",

		"Multiplier",
		"Life",
		"Health",
		"Armor",
		"GetOuttaJail",
		"CopBribe",
		"Invulnerability",
		"DoubleDamage",
		"FastReload",
		"ElectroFingers",
		"Respect",
		"Invisibility",
		"InstantAccess",

		"KillFrenzy",
		"DeletedBonus",
		"GTA2Badge",
		"Case",
	};

	private final int[] pickup_sprid = {
		569,
		577,
		585,
		593,
		601,
		609,
		617,
		625,
		633,
		641,
		649,
		657,
		658,
		659,
		660,
		661,
		669,
		677,
		685,
			690, //CarRocketL
			690, //CarWaterCannon
			690, //CarFlameThrower
			690, //LandRoamerMachinegun
			690, //InstantBomb
		698,
		699,
		700,
		701,
		702,
		710,
		718,
		726,
		734,
		742,
		750,
		758,
		766,
		774,
		782,
		790,
		798,

		837,
		1083,
		560,
		561,
	};

	public Pickup(int pickup, float x, float y, float z, int ang) {
		super(x, y, z, ang, 0);
		this.ang -= 90;
		spriteid = pickup_sprid[pickup];

		switch(pickup) {
			case 11:
			case 12:
			case 13:
			case 14:
			case 19:
			case 20:
			case 21:
			case 22:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 43:
				anim_len = 1;
			break;
			case 44:
				anim_len = 4;
			break;
			default:
				anim_len = 8;
			break;
		}
		if(pickup == 43)
			hasShadows = false;

		if(data[pickup] == null) {
			data[pickup] = new Texture[anim_len];
			data[pickup][0] = getTexture(spriteid);
			for(int i = 1; i < anim_len; i++)
				data[pickup][i] = getTexture(spriteid + i);
		}

		frames = data[pickup];
		palnum = Resources.paletteIndex[PaletteBase.SPRITE + spriteid];

		if(mesh == null) {
			float SIZEX = 0.5f;
			float SIZEY = 0.5f;
			float[] vertices = {
				-SIZEX,  0.1f,	SIZEY, 	0, 0, 1,1,1,1,
				SIZEX, 	 0.1f,	SIZEY, 	0, 1, 1,1,1,1,
			    SIZEX, 	 0.1f, 	-SIZEY, 1, 1, 1,1,1,1,
			    -SIZEX,  0.1f,  -SIZEY, 1, 0, 1,1,1,1,
			};
			short[] indicies = { 0, 1, 2, 2, 3, 0 };

			mesh = new Mesh(false,  vertices.length / 9,  indicies.length, VertexAttribute.Position(), VertexAttribute.TexCoords(0), VertexAttribute.ColorUnpacked() );
			mesh.setVertices(vertices);
	        mesh.setIndices(indicies);
	        vertices = null;
	        indicies = null;
		}
		scale = new Vector2(1, 1);
	}

	@Override
	public void render(ShaderProgram shader) {

		count += Gdx.graphics.getDeltaTime();
		if(count >= 0.1f) {
			count++;
			frame++;
			if(frame >= anim_len)
				frame = 0;

			count = 0;
		}


		Texture tex = frames[frame];
		tex.bind(0);

		scale.x = tex.getHeight() / 64f;
		scale.y = tex.getWidth() / 64f;

		transform.idt();
		transform.translate(pos);
		transform.rotate(0, 1, 0, ang);
		transform.scale(scale.x, 1, scale.y);

		Resources.palettes[palnum].getTexture().bind(1);
		shader.setUniformi("u_pal", 1);
		shader.setUniformMatrix("u_worldTrans", transform);
		shader.setUniformi("u_shadow", 0);
		mesh.render(shader, GL20.GL_TRIANGLES);
		if(Config.shadows && hasShadows) {
			shader.setUniformi("u_shadow", 1);
			mesh.render(shader, GL20.GL_TRIANGLES);
		}
	}

	@Override
	public void update(float delta) {


	}
}
