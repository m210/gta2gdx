//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

import java.nio.ByteBuffer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.BufferUtils;

public class Palette {
	private Texture pal;
	private Color[] color;

	public Palette(int[] RGB) {
		color = new Color[RGB.length];
		for(int i = 0; i < color.length; i++)
			color[i] = new Color(RGB[i]);
	}

	public Palette(int size) {
		color = new Color[size];
	}

	public int getSize() {
		return color.length;
	}

	public int getRGB(int index){
		return color[index].toIntBits();
	}

	public Color getColor(int index) {
		return color[index];
	}

	public float getRed(int index){
		return color[index].r;
	}

	public float getGreen(int index){
		return color[index].g;
	}

	public float getBlue(int index){
		return color[index].b;
	}

	public float getAlpha(int index){
		return color[index].a;
	}

	public void createPalette(int index, float red, float green, float blue) {
		color[index] = new Color(red, green, blue, 1.0f);
	}

	public void createPalette(int index, int red, int green, int blue) {
		color[index] = new Color(red / 255.0f, green / 255.0f, blue / 255.0f, 1.0f);
	}

	public void createPalette(int index, int RGB) {
		float red = ((RGB >> 16) & 0xFF) / 255f;
		float green = ((RGB >> 8) & 0xFF) / 255f;
		float blue = (RGB & 0xFF) / 255f;

		color[index] = new Color(red, green, blue, 1.0f);
	}

	public Texture getTexture() {
		if(pal == null) {
			ByteBuffer buf = BufferUtils.newByteBuffer(color.length*3);
			for(int i = 0; i < color.length; i++) {
				buf.put((byte) (color[i].r * 255));
				buf.put((byte) (color[i].g * 255));
				buf.put((byte) (color[i].b * 255));
			}
			buf.rewind();
			pal = new Texture(color.length, 1, Format.RGB888);
			Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, pal.getTextureObjectHandle());
			Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_RGB, color.length, 1, 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, buf);
			Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
		}
		return pal;
	}
}