//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

import java.nio.ByteBuffer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import ru.m210projects.gta2gdx.Resources;

public class FontBase {
	public int fontnum;
	public int base, sprbase;
	public int palnum;
	public Texture tex;
	public TextureRegion[] glyphs;

	public int glyphWidth;
	public int glyphHeight;
	public int glyphsPerRow = 12;

	//0 - 1366 144
	//1 - 1510 144
	//2 - 1654 144
	//3 - 1654 1
	//4 - 1654 1
	//5 - 1800 145
	//6 - 1945 144

	public FontBase(int fontnum, int base, int sprbase) {
		this.fontnum = fontnum;
		this.base = base;
		this.sprbase = sprbase;
		this.palnum = Resources.paletteIndex[PaletteBase.SPRITE + sprbase];

		glyphs = new TextureRegion[base];

		if(fontnum == 0)
			glyphWidth = 46;
		if(fontnum == 1)
			glyphWidth = 18;
		if(fontnum == 2)
			glyphWidth = 22;
		if(fontnum == 3)
			glyphWidth = 11;
		if(fontnum == 4)
			glyphWidth = 2;
		if(fontnum == 5)
			glyphWidth = 25;
		if(fontnum == 6)
			glyphWidth = 52;

		glyphHeight = Resources.sprite_entries[sprbase].h;

		tex = buildAtlas();
		glyphs[0] = new TextureRegion(tex, 10, 0);

		int x = 0;
        int y = 0;
        for(int i = 1; i < base; i++) {
        	int width = Resources.sprite_entries[sprbase + i - 1].w;
        	if(fontnum == 5)
        		width = Resources.sprite_entries[sprbase + i - 2].w;
        	int offset = (glyphWidth - width) / 2;
            glyphs[i] = new TextureRegion(tex, x + offset, y, glyphWidth - 2 * offset, glyphHeight);
            x += glyphWidth;
            if(x == glyphsPerRow * glyphWidth) {
                x = 0;
                y += glyphHeight;
            }
        }
	}

	public Texture buildAtlas() {
		int spriteid;
		int xlines = glyphsPerRow;
		if(base < xlines)
			xlines = base;
		int ylines = base / xlines + base % xlines;

		int fontsizeX = glyphWidth;
		int fontsizeY = glyphHeight;

		ByteBuffer buf = ByteBuffer.allocateDirect(xlines * fontsizeX * ylines * fontsizeY);
		byte[] pixel;

		for(int yl = 0; yl < ylines; yl++) {
			for (int y = 0; y < fontsizeY; y++) {
				for(int i = 0; i < xlines; i++) {
					spriteid = sprbase + i + yl * xlines;
					if(fontnum == 5) spriteid -= 1;
					int sprw = Resources.sprite_entries[spriteid].w;
					int ptr  = Resources.sprite_entries[spriteid].ptr;
					int xoffset = (fontsizeX - sprw) / 2;
					pixel = new byte[fontsizeX];
					if(xlines * yl + i < base)
						for (int x = 0; x < sprw; x++) {
							pixel[xoffset + x] = Resources.sprite_data[ptr + x + (y << 8)];
						}
					buf.put(pixel);
				}
			}
		}
		buf.rewind();

		Texture tex = new Texture(xlines * fontsizeX, ylines * fontsizeY, Format.RGB565);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_LUMINANCE, xlines * fontsizeX, ylines * fontsizeY, 0, GL20.GL_LUMINANCE, GL20.GL_UNSIGNED_BYTE, buf);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);

		pixel = null;
		buf.clear();
		return tex;
	}

	public void draw(SpriteBatch batcher, String text, float x, float y) {
        int len = text.length();
        for(int i = 0; i < len; i++) {
            int c = text.charAt(i) - ' ';

            if(c < 0 || c > glyphs.length - 1)
                continue;

            TextureRegion glyph = glyphs[c];

            batcher.draw(glyph, x, y);
            x += glyph.getRegionWidth();
        }
    }

	public Texture getTexture() {
		return tex;
	}
}
