//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.m210projects.gta2gdx.Resources;
import ru.m210projects.gta2gdx.tools.IOStream;
import ru.m210projects.gta2gdx.tools.LittleEndian;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.utils.IntArray;

public class StyReader {
	private IOStream io;
	private int chunkData;
	private String chunkHeader;
	private boolean debug = false;
	private int handle;

	static Map<String, Integer> chunkOffset = new HashMap<String, Integer>();
	static Map<String, Integer> chunkSize = new HashMap<String, Integer>();

	public StyReader(String path) {
		io = new IOStream();
		handle = init(path);
	}

	public int init(String path) {
		int fis = io.open(path);

		byte[] buf = new byte[4];

		io.read(fis, buf, buf.length);
		chunkHeader = new String(buf);
		if (!chunkHeader.equals("GBST")) {
			log("Not a GBH/GTA2 style format!");
			return -1;
		}

		buf = new byte[2];
		io.read(fis, buf, buf.length);
		chunkData = LittleEndian.getShort(buf);

		if (chunkData != 700) {
			log("Bad GTA2 style version (must be 700) = " + chunkData);
			log("Will ignore that for now, and read as if it was version 700...");
		}

		int offset = 0;
		buf = new byte[4];
		while(io.available(fis) > 0) {
			io.read(fis, buf, buf.length);
			chunkHeader = new String(buf);
			io.read(fis, buf, buf.length);
			offset = io.getcursor(fis);
			chunkData = LittleEndian.getInt(buf);
			chunkOffset.put(chunkHeader, offset);
			chunkSize.put(chunkHeader, chunkData);
			io.lseek(fis, chunkData, IOStream.SEEK_CUR);
		}
		return fis;
	}

	public short[] chunk_palindex() {
		io.lseek(handle, chunkOffset("PALX"), IOStream.SEEK_SET);
		chunkData = chunkData("PALX");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);

		short[] paletteIndex = new short[chunkData / 2];
		log("PALX: Reading " + chunkData / 2 + " pallete entries");

		for(int i = 0; i < paletteIndex.length; i++) {
			paletteIndex[i] = LittleEndian.getShort(buf, i*2);
		}
		return paletteIndex;
	}

	public Palette[] chunk_pals() {
		io.lseek(handle, chunkOffset("PPAL"), IOStream.SEEK_SET);
		chunkData = chunkData("PPAL");
		int pages_num = chunkData / (64 * 1024);
		log("PPAL: Reading " + pages_num +  " pallete pages (" + (chunkData / 1024) + " palletes)");
		Palette[] palettes = new Palette[chunkData / 1024];

		byte[] buf = new byte[256];
		for(int page = 0; page < pages_num; page++) {
			for(int index = 0; index < 256; index++) {
				io.read(handle, buf, buf.length);
				for(int pal = 0; pal < 64; pal++) {
					if(index == 0) palettes[(page << 6) + pal] = new Palette(256);
					int RGBA = LittleEndian.getInt(buf, pal*4);
					palettes[(page << 6) + pal].createPalette(index, RGBA);
				}
			}
		}
		return palettes;
	}

	public PaletteBase chunk_palb() {
		io.lseek(handle, chunkOffset("PALB"), IOStream.SEEK_SET);
		chunkData = chunkData("PALB");
		byte[] buf = new byte[chunkData];

		io.read(handle, buf, buf.length);

		log("PALB: Reading pallete bases...");

		int tile = LittleEndian.getUShort(buf, 0);
		int sprite = LittleEndian.getUShort(buf, 2);
		int car_remap = LittleEndian.getUShort(buf, 4);
		int ped_remap = LittleEndian.getUShort(buf, 6);
		int code_obj_remap = LittleEndian.getUShort(buf, 8);
		int map_obj_remap = LittleEndian.getUShort(buf, 10);
		int user_remap = LittleEndian.getUShort(buf, 12);
		int font_remap = LittleEndian.getUShort(buf, 14);

		PaletteBase palettebase = new PaletteBase(tile, sprite, car_remap, ped_remap, code_obj_remap, map_obj_remap, user_remap, font_remap);

		log("PALB: tile base : " + palettebase.tile);
		log("PALB: sprite base : " + palettebase.sprite);
		log("PALB: car_remap base : " + palettebase.car_remap);
		log("PALB: ped_remap base : " + palettebase.ped_remap);
		log("PALB: code_obj_remap base : " + palettebase.code_obj_remap);
		log("PALB: map_obj_remap base : " + palettebase.map_obj_remap);
		log("PALB: user_remap base : " + palettebase.user_remap);
		log("PALB: font_remap base : " + palettebase.font_remap);
		return palettebase;
	}

	public Texture chunk_tiles() {
		io.lseek(handle, chunkOffset("TILE"), IOStream.SEEK_SET);
		chunkData = chunkData("TILE");
		int tilesnum = chunkData / (64 * 64);
		log("TILE: Reading " + tilesnum + " tiles... ");

		byte[] tile_data = new byte[chunkData];
		io.read(handle, tile_data, tile_data.length);


		byte[] buf = new byte[256*256*8*4];
		ByteBuffer texArray = ByteBuffer.allocateDirect(2048*2048*4);
		Color color = new Color(0,0,0,0);

		int offset = 65536;
		for(int p1 = 0; p1 < 8; p1++) {
			int pos = 0;
			for(int y = 0; y < 256; y++) {
				for(int p2 = 0; p2 < 8; p2++) {
					int page = p2 + p1 * 8;
					for(int x = 0; x < 256; x++) {
						if(page < 62) {
							int index = page * offset + x + y *256;
							int pixel = tile_data[index] & 0xFF;
							color = Resources.palettes[Resources.paletteIndex[page << 4]].getColor(pixel);
							if(pixel == 0)
								color.a = 0;
						}
						buf[pos] = (byte) (color.r * 255);
						buf[pos + 1] = (byte) (color.g * 255);
						buf[pos + 2] = (byte) (color.b * 255);
						buf[pos + 3] = (byte) (color.a * 255);
						pos += 4;
					}
				}
			}
			texArray.put(buf);
		}

		tile_data = null;

		texArray.flip();

		Texture tex = new Texture(2048, 2048, Format.RGBA8888);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
		//Gdx.gl20.glGenerateMipmap(GL20.GL_TEXTURE_2D);  //Generate mipmaps now!!!
		//Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_NEAREST_MIPMAP_LINEAR);
		//Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MIN_FILTER, GL20.GL_NEAREST_MIPMAP_LINEAR);

		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_RGBA, 2048, 2048, 0, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, texArray);
		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
		tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		texArray.clear();
		System.gc();

		return tex;
	}

	public FontBase[] chunk_font() {
		io.lseek(handle, chunkOffset("FONB"), IOStream.SEEK_SET);
		chunkData = chunkData("FONB");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		int font_count = LittleEndian.getUShort(buf, 0);
		log("FONB: Found " + font_count + " fonts...");
		FontBase[] fbases = new FontBase[font_count];

		int base = LittleEndian.getUShort(buf, 2 + 2);
		int sprbase = Resources.spritebase.font_remap;
		fbases[0] = new FontBase(0, base, sprbase);
		log("FONB: Font " + 0 + " [" + base + " characters, sprbase "+ sprbase + "]");

		for (int i = 1; i < font_count; i++) {
			base = LittleEndian.getUShort(buf, 2 + i*2);
			sprbase += base;
			fbases[i] = new FontBase(i, base, sprbase);
			log("FONB: Font " + i + " [" + base + " characters, sprbase "+ sprbase + "]");
		}
		return fbases;
	}





	public SpriteEntry[] chunk_sprx() {
		io.lseek(handle, chunkOffset("SPRX"), IOStream.SEEK_SET);
		chunkData = chunkData("SPRX");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);

		SpriteEntry[] sprite_entries = new SpriteEntry[chunkData / 8];
		log("SPRX: Reading " + chunkData / 8 + " sprites...");


		for(int i = 0; i < chunkData / 8; i++) {
			int ptr = LittleEndian.getInt(buf, 8 * i);
			int w = LittleEndian.getUByte(buf, 8 * i + 4);
			int h = LittleEndian.getUByte(buf, 8 * i + 5);
			int pad = LittleEndian.getUShort(buf, 8 * i + 6);

			sprite_entries[i] = new SpriteEntry(ptr, w, h, pad);
		}
		return sprite_entries;
	}

	public byte[] chunk_sprg() {
		io.lseek(handle, chunkOffset("SPRG"), IOStream.SEEK_SET);
		chunkData = chunkData("SPRG");
		byte[] sprite_data = new byte[chunkData];
		io.read(handle, sprite_data, sprite_data.length);
		log("SPRG: Reading sprite graphics");
		return sprite_data;
	}

	public SpriteBase chunk_sprb() {
		io.lseek(handle, chunkOffset("SPRB"), IOStream.SEEK_SET);
		chunkData = chunkData("SPRB");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		log("SPRB: Reading sprite bases....");

		int car = 0;
		int ped = car + LittleEndian.getUShort(buf, 0);
		int code_obj = ped + LittleEndian.getUShort(buf, 2);
		int map_obj = code_obj + LittleEndian.getUShort(buf, 4);
		int user = map_obj + LittleEndian.getUShort(buf, 6);
		int font = user + LittleEndian.getUShort(buf, 8);

		SpriteBase spritebase = new SpriteBase(car, ped, code_obj, map_obj, user, font);

		log("SPRB: car base : " + spritebase.car_remap);
		log("SPRB: ped base : " + spritebase.ped_remap);
		log("SPRB: code_obj base :" + spritebase.code_obj_remap);
		log("SPRB: map_obj base : " + spritebase.map_obj_remap);
		log("SPRB: user base : " + spritebase.user_remap);
		log("SPRB: font base : " + spritebase.font_remap);
		return spritebase;
	}

	public List<DeltaIndex> chunk_deltax() {
		io.lseek(handle, chunkOffset("DELX"), IOStream.SEEK_SET);
		chunkData = chunkData("DELX");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		log("DELX: Reading delta indexes...");

		List<DeltaIndex> deltax = new ArrayList<DeltaIndex>();
		int pos = 0;
		int doffset = 0;
		while(pos < chunkData) {
			int sprite = LittleEndian.getUShort(buf, pos);
			pos += 2;
			int count = LittleEndian.getUByte(buf, pos);
			pos += 2;
			IntArray size = new IntArray();
			int offset = 0;
			for (int j = 0; j < count; j++)  {
				int var = LittleEndian.getUShort(buf, pos);
				offset += var;
                size.add(var);
                pos += 2;
			}
			DeltaIndex delta = new DeltaIndex(sprite, size, doffset);
			doffset += offset;
			deltax.add(delta);
		}
		log("DELX: Found " + deltax.size() + " delta indexes");
		return deltax;
	}

	public byte[] chunk_deltas() {
		io.lseek(handle, chunkOffset("DELS"), IOStream.SEEK_SET);
		chunkData = chunkData("DELS");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		log("DELS: Reading delta stores...");
		return buf;
	}

	public List<CarInfo> chunk_cars() {
		io.lseek(handle, chunkOffset("CARI"), IOStream.SEEK_SET);
		chunkData = chunkData("CARI");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		log("CARI: Reading car info...");

		List<CarInfo> cars_infos = new ArrayList<CarInfo>();

		int pos = 0;
		while(pos < chunkData) {
			CarInfo car = new CarInfo();
			car.model = buf[pos++] & 0xFF;
			car.sprite = buf[pos++] & 0xFF;
			car.w = buf[pos++] & 0xFF;
			car.h = buf[pos++] & 0xFF;
			car.num_remaps = buf[pos++] & 0xFF;
			car.passengers = buf[pos++] & 0xFF;
			car.wreck = buf[pos++] & 0xFF;
			car.rating = buf[pos++] & 0xFF;
			car.front_wheel_offset = buf[pos++];
			car.rear_wheel_offset = buf[pos++];
			car.front_window_offset = buf[pos++];
			car.rear_window_offset = buf[pos++];
			car.info_flags = buf[pos++] & 0xFF;
			car.info_flags_2 = buf[pos++] & 0xFF;
			/*
			    var infoFlags2Value0 = BitHelper.CheckBit(infoFlag2, 0);
                var infoFlags2Value1 = BitHelper.CheckBit(infoFlag2, 1);
                if (infoFlags2Value0)
                    carInfo.InfoFlags += 0x100;
                if (infoFlags2Value1)
                    carInfo.InfoFlags += 0x200;
			 */

			if(car.num_remaps > 0)
				car.remap = new int[car.num_remaps];
			for(int i = 0; i < car.num_remaps; i++)
				car.remap[i] = buf[pos++] & 0xFF;
			car.num_doors = buf[pos++] & 0xFF;
			car.doors = new CarInfo.DoorInfo[car.num_doors];
			for(int i = 0; i < car.num_doors; i++) {
				int x = buf[pos++];
				int y = buf[pos++];
				car.doors[i] = new CarInfo.DoorInfo(x, y);
			}
			cars_infos.add(car);
		}
		log("CARI: Found " + cars_infos.size() + " cars info");
		return cars_infos;
	}

	public IntArray chunk_reci() {
		io.lseek(handle, chunkOffset("RECY"), IOStream.SEEK_SET);
		chunkData = chunkData("RECY");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);

		IntArray carrec_info = new IntArray();

		log("RECY: Read car recycling info chunk...");

		for (int i = 0; i < chunkData; i++) {
            int value = buf[i] & 0xff;
            if (value == 255)
                break;
            carrec_info.add(value);
        }
		return carrec_info;
	}

	public List<ObjectInfo> chunk_objects() {
		io.lseek(handle, chunkOffset("OBJI"), IOStream.SEEK_SET);
		chunkData = chunkData("OBJI");
		byte[] buf = new byte[chunkData];
		io.read(handle, buf, buf.length);
		log("OBJI: Reading map object information..");

		List<ObjectInfo> obj_infos = new ArrayList<ObjectInfo>();

		for(int i = 0; i < chunkData; i += 2)
			obj_infos.add(new ObjectInfo(buf[i]  & 0xff, buf[i + 1]  & 0xff));

		log("OBJI: Found " + obj_infos.size() + " objects info");
		return obj_infos;
	}

	public void close() {
		io.close(handle);
	}


//	public Texture load_texpage(int page) {
//		io.lseek(handle, chunkOffset("TILE") + page * 65536, IOStream.SEEK_SET);
//		chunkData = chunkData("TILE");
//
//		byte[] tile_data = new byte[256*256];
//		io.read(handle, tile_data, tile_data.length);
//
//		ByteBuffer buf = BufferUtils.newByteBuffer(256*256*4);
//		for(int i = 0; i < tile_data.length; i++) {
//			int pixel = tile_data[i] & 0xFF;
//			Color color = Resources.palettes[Resources.paletteIndex[page << 4]].getColor(pixel);
//
//			buf.put((byte) (color.r * 255));
//			buf.put((byte) (color.g * 255));
//			buf.put((byte) (color.b * 255));
//			buf.put((byte) (color.a * 255));
//		}
//		buf.position(0);
//
//		Texture tex = new Texture(256, 256, Format.RGBA8888);
//		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
//		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_RGBA, 256, 256, 0, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, buf);
//		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
//
//		return tex;
//	}

//	public Texture load_sprpage(int page) {
//		io.lseek(handle, chunkOffset("SPRG") + page * 65536, IOStream.SEEK_SET);
//		chunkData = chunkData("SPRG");
//
//		byte[] tile_data = new byte[256*256];
//		io.read(handle, tile_data, tile_data.length);
//
//		ByteBuffer buf = BufferUtils.newByteBuffer(256*256*4);
//		for(int i = 0; i < tile_data.length; i++) {
//			int pixel = tile_data[i] & 0xFF;
//			Color color = Resources.palettes[0].getColor(pixel);
//
//			buf.put((byte) (color.r * 255));
//			buf.put((byte) (color.g * 255));
//			buf.put((byte) (color.b * 255));
//			buf.put((byte) (color.a * 255));
//		}
//		buf.position(0);
//
//		Texture tex = new Texture(256, 256, Format.RGBA8888);
//		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
//		Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_RGBA, 256, 256, 0, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, buf);
//		Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
//
//		return tex;
//	}


	private int chunkOffset(String chunk) {
		int out = 0;
		for(Map.Entry<String, Integer> entry : chunkOffset.entrySet()) {
			if(entry.getKey().equals(chunk)) {
				out = entry.getValue();
			}
		}
		return out;
	}

	private int chunkData(String chunk) {
		int out = 0;
		for(Map.Entry<String, Integer> entry : chunkSize.entrySet()) {
			if(entry.getKey().equals(chunk)) {
				out = entry.getValue();
			}
		}
		return out;
	}

	private void log(String txt) {
		if(debug)
			System.out.println(txt);
	}
}
