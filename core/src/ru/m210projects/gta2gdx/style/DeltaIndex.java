//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

import com.badlogic.gdx.utils.IntArray;

public class DeltaIndex {
	public int which_sprite;
	public IntArray size;
	public int offset;

	public DeltaIndex(int which_sprite, IntArray size, int offset) {
		this.which_sprite = which_sprite;
		this.size = size;
		this.offset = offset;
	}
}
