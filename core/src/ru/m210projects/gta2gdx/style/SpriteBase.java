//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

public class SpriteBase {
	public int car_remap, ped_remap, code_obj_remap, map_obj_remap, user_remap, font_remap;

	public static int CAR;
	public static int PED;
	public static int CODE_OBJ;
	public static int MAP_OBJ;
	public static int USER;
	public static int FONT;

	public SpriteBase(int car, int ped, int code_obj, int map_obj, int user, int font) {
		this.car_remap = car;
		this.ped_remap = ped;
		this.code_obj_remap = code_obj;
		this.map_obj_remap = map_obj;
		this.user_remap = user;
		this.font_remap = font;

    	CAR = car_remap;
     	PED = ped_remap;
    	CODE_OBJ = code_obj_remap;
    	MAP_OBJ = map_obj_remap;
    	USER = user_remap;
    	FONT = font_remap;
	}
}
