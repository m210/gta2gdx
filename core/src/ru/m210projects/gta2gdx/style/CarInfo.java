//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

public class CarInfo {

	public int model;
	public int sprite;
	public int w, h;
	public int num_remaps;
	public int passengers;
	public int wreck;
	public int rating;
	public int front_wheel_offset, rear_wheel_offset;
	public int front_window_offset, rear_window_offset;
	public int info_flags;
	public int info_flags_2;
	public int[] remap;
	public int num_doors;
	public DoorInfo[] doors;

	public static class DoorInfo {
		int rx;
		int ry;

		public DoorInfo(int rx, int ry) {
			this.rx = rx;
			this.ry = ry;
		}
	}
}
