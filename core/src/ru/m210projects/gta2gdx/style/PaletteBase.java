//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.style;

public class PaletteBase {
	public int tile, sprite, car_remap, ped_remap, code_obj_remap, map_obj_remap, user_remap, font_remap;

	public static int SPRITE;
	public static int CAR;
	public static int PED;
	public static int CODE_OBJ;
	public static int MAP_OBJ;
	public static int USER;
	public static int FONT;

	public PaletteBase(int tile, int sprite, int car, int ped, int code_obj, int map_obj, int user, int font) {
		this.tile = tile;
		this.sprite = sprite;
		this.car_remap = car;
		this.ped_remap = ped;
		this.code_obj_remap = code_obj;
		this.map_obj_remap = map_obj;
		this.user_remap = user;
		this.font_remap = font;

		SPRITE =  tile;
    	CAR = SPRITE + sprite;
     	PED = CAR + car_remap;
    	CODE_OBJ = PED + ped_remap;
    	MAP_OBJ = CODE_OBJ + code_obj_remap;
    	USER = MAP_OBJ + map_obj_remap;
    	FONT = USER + user_remap;
	}
}
