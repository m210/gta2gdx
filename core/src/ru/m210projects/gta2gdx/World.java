//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.objects.Car;
import ru.m210projects.gta2gdx.objects.Ped;
import ru.m210projects.gta2gdx.objects.Player;
import ru.m210projects.gta2gdx.objects.Sprite;
import ru.m210projects.gta2gdx.script.ScrReader;
import ru.m210projects.gta2gdx.tools.CarModels;

import com.badlogic.gdx.math.Vector3;


public class World {

	public static final int PARTSIZE = 16;
	public static final int MAPPARTS = 18;

	Resources res;
	ScrReader scr;

	public List<Sprite> obj = new ArrayList<Sprite>();

	public Player player;
	public static Random rnd;
	public Vector3 spawnAxis = new Vector3();

	public int ped_spawnradius = 5;
	public boolean ped_spawning = false;
	public static final int ped_onscreen = 40;
	public List<Sprite> peds = new ArrayList<Sprite>();
	public float ped_spawncount = 0;

	public int car_spawnradius = 8;
	public boolean car_spawning = false;
	public List<Sprite> cars = new ArrayList<Sprite>();
	public static final int cars_onscreen = 16;
	public float car_spawncount = 0;

	public static boolean[][] road_blocked = new boolean[256][256];
	public static TrafficManager traf_mng;


	public World() {
		scr = new ScrReader(Resources.mapname + ".scr", this);
		traf_mng = new TrafficManager(this);

		for(int i = 0; i < ped_onscreen; i++)
			peds.add(new Ped(0, 0, 0, 0, 0, i));
		for(int i = 0; i < cars_onscreen; i++)
			cars.add(new Car());

		/*
		for(int i = 0; i < 45; i++)
			obj.add(new Pickup(i, player.pos.x + 11, player.pos.z - 10-i*0.5f, player.pos.y, 0));
			*/
		rnd = new Random();
	}

	public void update(float delta) {
		traf_mng.update(delta);



		for(int i = 0; i < obj.size(); i++) {
			Sprite spr = obj.get(i);
			if(spr.name != null && spr.name.equals("Car")) {
				Car car = (Car) spr;

				int x = (int)(car.pos.x - World.PARTSIZE);
				int y = (int)(car.pos.z - World.PARTSIZE);


				//road_blocked[x + (int)(car.getDirection().x)][y - (int)(car.getDirection().y)] = true;
				road_blocked[x][y] = true;
				road_blocked[x - (int)(car.getDirection().x * car.sprh / 64f)][y + (int)(car.getDirection().y * car.sprh / 64f)] = true;
			}
			if(spr.name != null && (spr.name.equals("Ped") || spr.name.equals("Player"))) {
				Ped ped = (Ped) spr;

				int x = (int)(ped.pos.x - World.PARTSIZE);
				int y = (int)(ped.pos.z - World.PARTSIZE);
				if(getRoad(ped.pos.x, ped.pos.z) != -1)
					road_blocked[x][y] = true;
			}
		}

		for(int i = 0; i < obj.size(); i++) {
			Sprite spr = obj.get(i);
			if(spr != null) {
				spr.update(delta);

				if(spr.name != null && spr.name.equals("Ped")) {
					Vector3 pedPos = spr.pos;
					Vector3 plPos = player.pos;
					if(Math.abs(pedPos.x - plPos.x) > ped_spawnradius || Math.abs(pedPos.z - plPos.z) > ped_spawnradius) {
						peds.add(spr);
						obj.remove(spr);
					}
				}

				if(spr.name != null && spr.name.equals("Car")) {
					Car car = (Car) spr;
					Vector3 carPos = car.pos;
					Vector3 plPos = player.pos;
					if(!car.isParked() && (Math.abs(carPos.x - plPos.x) > 10 || Math.abs(carPos.z - plPos.z) > 10)) {
						cars.add(spr);
						obj.remove(spr);
					}
				}
			}
		}


		if((ped_onscreen - peds.size()) < ped_onscreen) {
			ped_spawning = true;
		}

		if((cars_onscreen - cars.size()) < cars_onscreen) {
			car_spawning = true;
		}


		if(car_spawning) {
			car_spawncount += delta;
			if(car_spawncount > 0.25f) {
				for(int ang = 0; ang < 360; ang += 90) {
					spawnAxis = getSpawncoords(ang);
					float spawnx = (int) player.pos.x + car_spawnradius * spawnAxis.x;
					float spawny = (int) player.pos.z - car_spawnradius * spawnAxis.y;
					int i = rnd.nextInt(17);
					float x = spawnx + (i - 8) * spawnAxis.y;
					float y = spawny + (i - 8) * spawnAxis.x;
					int z = getRoad(x, y);
					if(z != -1) {
						if(x < road_blocked.length && y < road_blocked[(int)x].length && !road_blocked[(int)x][(int)y] && !traf_mng.isZone(x,y)) {
							BlockInfo roadBlock = Resources.getBlock((int) x - PARTSIZE, (int)y - PARTSIZE, z);
							int arrows = roadBlock.arrows & 0xFF;
							if(arrows != 0 && arrows <= 8) {
								int roadDirection = 0;
								if((arrows & 1) != 0)
									roadDirection = 180;
								if((arrows & 4) != 0)
									roadDirection = 90;
								if((arrows & 8) != 0)
									roadDirection = 270;

								if(ang == roadDirection || ang == (roadDirection + 180) % 360) {
									if(cars.size() > 0) {
										Car car = (Car) cars.get(0);
										car.pos.x = (int)x + 0.5f;
										car.pos.z = (int)y + 0.5f;
										car.pos.y = z+1;
										car.ang = roadDirection;
										car.changeModel(rnd.nextInt(CarModels.NUMMODELS));
										car.changePal(rnd.nextInt(50));
										obj.add(car);
										cars.remove(0);
										car_spawning = false;
									}
								}
							}
						}
					}
				}
				car_spawncount = 0;
			}
		}

		if(ped_spawning) {
			ped_spawncount += delta;
			int spawnang = rnd.nextInt(360);
			if(player.getSpeed() > 0) {
				spawnang = (int) player.ang;
			}

			if(ped_spawncount > 0.1f) {
				spawnAxis = getSpawncoords(spawnang);
				float spawnx = player.pos.x + ped_spawnradius * spawnAxis.x;
				float spawny = player.pos.z - ped_spawnradius * spawnAxis.y;

				int i = rnd.nextInt(9);
				float x = spawnx + (i - 4) * spawnAxis.y;
				float y = spawny + (i - 4) * spawnAxis.x;
				int z = getGround(x, y) + 1;
				if(z > 0  && x < road_blocked.length && y < road_blocked[(int)x].length && !road_blocked[(int)x][(int)y]) {
					if(peds.size() > 0) {
						Ped ped = (Ped) peds.get(0);
						ped.pos.x = x;
						ped.pos.z = y;
						ped.pos.y = z;
						ped.ang = (int) ((spawnAxis.z + 180) % 360);
						obj.add(ped);
						peds.remove(0);
						ped_spawning = false;
					}
				}
				ped_spawncount = 0;
			}
		}




//		for(int x = 0; x < 256; x++)
//			for(int y = 0; y < 256; y++)
//				road_blocked[x][y] = false;
	}









	public Vector3 getSpawncoords(int ang) {
		int ax = 1;
		int ay = 0;
		int aang = 0;
		if(ang > 45 && ang < 135) {
			ax = 0;
			ay = 1;
			aang = 90;
		} else if(ang > 135 && ang < 225) {
			ax = -1;
			ay = 0;
			aang = 180;
		} else if(ang > 225 && ang < 315) {
			ax = 0;
			ay = -1;
			aang = 270;
		}
		spawnAxis.set(ax, ay, aang);
		return spawnAxis;
	}

	public static int getGround(float x, float y) {
		x -= PARTSIZE;
		y -= PARTSIZE;
		if(x < 0 || y < 0 || x >= 256 || y >= 256)
			return -1;

		return Resources.groundz[(int)x][(int)y];
	}

	public static int getRoad(float x, float y) {
		x -= PARTSIZE;
		y -= PARTSIZE;
		if(x < 0 || y < 0 || x >= 256 || y >= 256)
			return -1;

		return Resources.roadz[(int)x][(int)y];
	}

	public static boolean isBlock(float x, float y, float z) {
		BlockInfo block = Resources.getBlock((int)x, (int)y, (int)z);
		if(block != null) {
			if(block.slope_type == 0 && block.arrows == 0 && block.lid == 0 &&
			block.top == 0 && block.bottom == 0 && block.left == 0 && block.right == 0)
				return false;
		}
		return (block != null);
	}

	public static int getZ(float x, float y) {
		if(x < 0 || y < 0 || x >= 256 || y >= 256)
			return 0;

		for(int z = 7; z >= 0; z--) {
			BlockInfo block = Resources.getBlock((int)x, (int)y, z);
			if(block != null && block.ground_type != BlockInfo.AIR) {
				return z + 1;
			}
		}
		return 0;
    }
}
