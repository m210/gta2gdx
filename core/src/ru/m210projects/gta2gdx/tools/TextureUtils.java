//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

import com.badlogic.gdx.math.Vector2;

public class TextureUtils {
	private static final int atlasSize = 32;
	private static final int pagesize = 4;
	private static final float texelOffset = 1f / 4096;

	private static final int[][] texcoord_rotation = {
		//Normal face rotation
		{3,0,2,1},//0
		{2,3,1,0},//90
		{1,2,0,3},//180
		{0,1,3,2},//270
		//Flipped face rotation
		{2,1,3,0},//0
		{3,2,0,1},//90
		{0,3,1,2},//180
		{1,0,2,3},//270
	};

	public static Vector2[] calculateUVMapping(int tileid) {
		boolean flip = BitHelper.CheckBit(tileid, 13);
    	boolean bit14 = BitHelper.CheckBit(tileid, 14);
    	boolean bit15 = BitHelper.CheckBit(tileid, 15);

		tileid &= ~(1 << 10 | 1 << 11 | 1 << 12 | 1 << 13 | 1 << 14 | 1 << 15) & 0xffff;

		int pagenum = tileid >> 4;
		int pagetile = tileid % 16;

    	int pu = pagenum % 8;
    	int pv = pagenum >> 3;

        int u = pu * 4 + pagetile % pagesize;
        int v = pv * 4 + pagetile / pagesize;

        float xOffset = 1f / atlasSize;
        float yOffset = 1f / atlasSize;

        float uOffset = (u * xOffset);
        float vOffset = (v * yOffset);

        Vector2[] UVList = new Vector2[4];

        int angle = 0;

        if (!bit14 && !bit15) {
        	if(flip)
        		angle = 4;
        }
        if (bit14 && !bit15) {
            angle = 1; //90
            if(flip)
        		angle = 5;
        }
        if (!bit14 && bit15) {
        	angle = 2; //180
        	if(flip)
        		angle = 6;
        }
        if (bit14 && bit15) {
        	angle = 3; //270
        	if(flip)
        		angle = 7;
        }

        UVList[texcoord_rotation[angle][0]] = new Vector2(uOffset + texelOffset, vOffset + texelOffset); // 0,0
        UVList[texcoord_rotation[angle][1]] = new Vector2(uOffset + texelOffset, vOffset + yOffset - texelOffset); // 0,1
        UVList[texcoord_rotation[angle][2]] = new Vector2(uOffset + xOffset - texelOffset, vOffset + texelOffset); // 1,0
        UVList[texcoord_rotation[angle][3]] = new Vector2(uOffset + xOffset - texelOffset, vOffset + yOffset - texelOffset); // 1,1

        return UVList;
    }
}
