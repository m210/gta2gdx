//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

public class DeltaType {

	public static enum Type {

		RearRightDent(0, "rear right dent"),
		RearLeftDent(1, "rear left dent"),
		FrontLeftDent(2, "front left dent"),
		FrontRightDent(3, "front right dent"),
		WindscreenDamage(4, "windscreen damage"),
		LeftBrakeLight(5, "left brake light"),
		LeftHeadlight(6, "left headlight"),
		LeftFrontDoor1(7, "left front door (almost closed)"),
		LeftFrontDoor2(8, "left front door (slightly opened)"),
		LeftFrontDoor3(9, "left front door (almost open)"),
		LeftFrontDoor4(10, "left front door (open)"),
		LeftBackDoor1(11, "left back door (almost closed)/FBI light animation"),
		LeftBackDoor2(12, "left back door (slightly opened)/FBI light animation"),
		LeftBackDoor3(13, "left back door (almost open)/FBI light animation"),
		LeftBackDoor4(14, "left back door (open)/FBI light animation"),
		RoofLights(15, "emergency/roof lights/decal"),
		EmergencyLights(16, "emergency lights"),
		RightRearEmergencyLight(17, "right rear emergency light"),
		LeftRearEmergLight(18, "Left rear emergency light"),
		RacingCarNumber(19, "Racing Car (Sprite #17) roof No. 8"),
		RacingCarNumber2(20, "Racing Car (Sprite #17) roof No. 9"),
		RightBrakeLight(22, "right brake light (mirror)"),
		RightHeadlight(23, "right headlight (mirror)"),
		RightFrontDoor1(24, "right front door (almost closed) (mirror)"),
		RightFrontDoor2(25, "right front door (slightly opened) (mirror)"),
		RightFrontDoor3(26, "right front door (almost open) (mirror)"),
		RightFrontDoor4(27, "right front door (open) (mirror)"),
		RightBackDoor1(28, "right back door (almost closed) (mirror)"),
		RightBackDoor2(29, "right back door (slightly opened) (mirror)"),
		RightBackDoor3(30, "right back door (almost open) (mirror)"),
		RightBackDoor4(31, "right back door (open) (mirror)");

		private int value;
		//private String description;

		private Type(int value, String description) {
			this.value = value;
			//this.description = description;
		}

		public int getValue() {
			return value;
		}
	}
}
