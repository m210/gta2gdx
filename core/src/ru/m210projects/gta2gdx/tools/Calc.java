//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

public class Calc {
	public static boolean inRegion(float point_x, float point_y, float lx, float ly, float rx, float ry) {
		return !(point_y > ry || point_y < ly || point_x < lx || point_x > rx);
	}

	public static boolean intersect(float ax, float ay, float ax1, float ay1, float bx, float by, float bx1, float by1) {
		return !( ay > by1 || ay1 < by || ax1 < bx || ax > bx1 );
	}
}
