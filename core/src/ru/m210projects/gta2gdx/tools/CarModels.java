//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

import ru.m210projects.gta2gdx.Resources;

public class CarModels {
	public static int NUMMODELS = 128;
	public static int[] sprite_array = new int[NUMMODELS];
	public static int[] model_index = new int[NUMMODELS];

	public static void initCars() {
		int submodel = 0;
		for(int i = 0; i < Resources.cars_infos.size(); i++) {
			int model = Resources.cars_infos.get(i).model;

			if(Resources.cars_infos.get(i).sprite == 0)
				submodel++;

			model_index[model] = i;
			sprite_array[model] = i - submodel;
		}
	}
}
