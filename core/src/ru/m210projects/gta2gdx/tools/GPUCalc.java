//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

import java.nio.ByteBuffer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.ScreenUtils;

public class GPUCalc {
	private FrameBuffer frameBuffer = null;
	private TextureRegion m_fboRegion = null;
	private Texture datatexture;
	private Pixmap datapixmap;

	private byte[] array;
	private int width;
	private int height;
    private int fbowidth;
    private int fboheight;

    private ShaderProgram shader;
    SpriteBatch spriteBatch;
    /*
        byte[] array = new byte[2000*4];
		gpu = new GPUCalc(array, array.length / 4, 1);
		gpu.calc(batch);
		img = gpu.outputT();
    */

    public GPUCalc(byte[] array, int width, int height) {
    	this.array = array;
    	this.width = width;
    	this.height = height;

    	fbowidth = Gdx.graphics.getWidth();
        fboheight = Gdx.graphics.getHeight();

    	frameBuffer = new FrameBuffer(Format.RGBA8888, width, height, false);
    	m_fboRegion = new TextureRegion(frameBuffer.getColorBufferTexture());
	    m_fboRegion.flip(false, true);


	    String vertexShader = "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
				+ "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
				+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
				+ "uniform mat4 u_projTrans;\n" //
				+ "varying vec4 v_color;\n" //
				+ "varying vec2 v_texCoords;\n" //
				+ "\n" //
				+ "void main()\n" //
				+ "{\n" //
				+ "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
				+ "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
				+ "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
				+ "}\n";

	    String fragmentShader = Gdx.files.internal("shaders/gpu_fragment.glsl").readString();

		shader = new ShaderProgram(vertexShader, fragmentShader);
		if (shader.isCompiled() == false) throw new IllegalArgumentException("couldn't compile shader: " + shader.getLog());

		spriteBatch = new SpriteBatch();
    }

    public void calc() {
    	array_toTexture();
    	frameBuffer.begin();
    	spriteBatch.setShader(shader);
 	    spriteBatch.begin();
 	    spriteBatch.draw(datatexture, 0, 0, fbowidth, fboheight);
 	    spriteBatch.end();
 	    spriteBatch.setShader(null);
 	    datapixmap = ScreenUtils.getFrameBufferPixmap(0, 0, width, height);
 	    frameBuffer.end();
    }

	public Texture outputT() {
		datatexture = new Texture(datapixmap);
		return datatexture;
	}

	public byte[] output() {
		ByteBuffer out = datapixmap.getPixels();
 	    byte[] buf = new byte[out.remaining()];
 	    out.get(buf, 0, buf.length);
 	    out.rewind();
 		dispose();
 	    return buf;
	}

	public void dispose() {
		datapixmap.dispose();
 	    datatexture = null;
 	    frameBuffer = null;
 	    m_fboRegion = null;
 	    array = null;
	}

	public void array_toTexture() {
		if(array != null) {
			ByteBuffer texArray = ByteBuffer.allocateDirect(array.length);
			texArray.put(array);
			texArray.rewind();
			datatexture = new Texture(width, height, Format.RGBA8888);
			Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, datatexture.getTextureObjectHandle());
			Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_ALPHA, width, height, 0, GL20.GL_ALPHA, GL20.GL_UNSIGNED_BYTE, texArray);
			Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_2D, 0);
			datatexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
			texArray.clear();
		}
	}
}

