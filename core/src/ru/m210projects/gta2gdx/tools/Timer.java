//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

public class Timer {
	static long startTime;
	static long spentTime;

	public static void start() {
		startTime = System.currentTimeMillis();
	}

	public static long result() {
		spentTime = System.currentTimeMillis() - startTime;
		System.out.println(spentTime +" msec");
		return spentTime;
	}

	public static long result(String comment) {
		spentTime = System.currentTimeMillis() - startTime;
		System.out.println(comment + " : " + spentTime +" msec");
		return spentTime;
	}

	public static void startFPS() {
		startTime = System.nanoTime();
	}

	public static int FPSresult() {
		spentTime = ((System.nanoTime() - startTime));
		long fps = (long) (1000000000.0/spentTime);
		System.out.println(fps +" fps");
		return (int) fps;
	}
}
