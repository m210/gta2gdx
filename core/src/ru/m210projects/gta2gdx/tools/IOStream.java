//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx.tools;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;


public class IOStream {
	public static final int SEEK_SET = 0;
	public static final int SEEK_CUR = 1;
	public static final int SEEK_END = 2;
	FileHandle handle;

	List<RandomAccessFile> opened_files = new ArrayList<RandomAccessFile>();

	public int open(String filename) {
		int var = -1;
		try {
			handle =  Gdx.files.external("data" + File.separator + filename);
			opened_files.add(new RandomAccessFile(handle.file(), "r"));
			var = opened_files.size();
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load file " + handle.file().getAbsolutePath());
		}
		return var;
	}

	public int close(int handle) {
		int var = -1;
		RandomAccessFile file = opened_files.get(handle - 1);
		try {
			if (file != null) {
				file.close();
				opened_files.remove(handle - 1);
				var = 0;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return var;
	}

	public int lseek(int handle, long offset, int whence) {
		int var = -1;
		RandomAccessFile fis = opened_files.get(handle - 1);
		try {
			if(whence == SEEK_SET) {
				fis.seek(offset);
			} else if(whence == SEEK_CUR) {
				fis.skipBytes((int) offset);
			} else if(whence == SEEK_END) {
				fis.getChannel().position(fis.getChannel().size() + offset);
			}

			var = (int) fis.getChannel().position();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Couldn't load file " + handle);
	    }
		return var;
	}

	public int getcursor(int handle) {
		int var = -1;
		RandomAccessFile fis = opened_files.get(handle - 1);
		try {
			var = (int) fis.getChannel().position();
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load file " + handle);
		}
		return var;
	}


	public int available(int handle) {
		int var = -1;
		RandomAccessFile fis = opened_files.get(handle - 1);
		try {
			var = (int) (fis.length() - fis.getFilePointer());
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load file " + handle);
		}
		return var;
	}


	public int read(int handle, byte[] buf, int len) {
		int var = -1;

		RandomAccessFile fis = opened_files.get(handle - 1);
		try {
			fis.read(buf, 0, len);
			var = len;
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load file " + handle);
	    }
		return var;
	}


	public int read(int handle, ByteBuffer mBuf) {
		int var = -1;
		FileChannel fChan;
		RandomAccessFile fis = opened_files.get(handle - 1);
		fChan = fis.getChannel();
		try {
		    fChan.read(mBuf, getcursor(handle));
		    mBuf.rewind();
		    var = mBuf.capacity();
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load file " + handle);
	    }
		return var;
	}

}
