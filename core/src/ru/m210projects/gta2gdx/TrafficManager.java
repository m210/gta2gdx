//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx;

import java.util.List;

import ru.m210projects.gta2gdx.map.BlockInfo;
import ru.m210projects.gta2gdx.map.Zone;
import ru.m210projects.gta2gdx.objects.TrafLight;

import com.badlogic.gdx.math.Vector2;

public class TrafficManager {
	public boolean[][] traffic_zone = new boolean[256][256];
	public int trafficDirection = 1;
	public float trafficCount = 0;

	public TrafficManager(World world) {
		List<Zone> zones = Resources.zones;
		for(int i = 0; i < zones.size(); i++) {
			Zone zone = zones.get(i);
			if(zone.getType() == 2) {
				Vector2 pos = zone.getPosition();
				Vector2 dim = zone.getDimensions();

				for(int j = 0; j < dim.x; j++)
					for(int k = 0; k < dim.y; k++)
						traffic_zone[(int)pos.x + j][(int)pos.y + k] = true;

				float z = World.getZ(pos.x, pos.y);

				float x = pos.x + dim.x / 2;
				float y = pos.y - 1;
				if(!World.isBlock(x,y,z) && (World.isBlock(x, y, z -1 ) && Resources.getBlock((int) x, (int) y, (int) z - 1).ground_type != BlockInfo.PAVEMENT)) {
					TrafLight tr = new TrafLight(x + World.PARTSIZE, y + World.PARTSIZE, z + 2, 90);
					world.obj.add(tr);
				}
				x = pos.x + dim.x / 2;
				y = pos.y + dim.y;
				if(!World.isBlock(x,y,z) && (World.isBlock(x, y, z -1 ) && Resources.getBlock((int) x, (int) y, (int) z - 1).ground_type != BlockInfo.PAVEMENT)) {
					TrafLight tr = new TrafLight(x + World.PARTSIZE, y + World.PARTSIZE + 1, z + 2, 270);
					world.obj.add(tr);
				}
				x = pos.x - 1;
				y = pos.y + dim.y / 2;
				if(!World.isBlock(x,y,z) && (World.isBlock(x, y, z -1 ) && Resources.getBlock((int) x, (int) y, (int) z - 1).ground_type != BlockInfo.PAVEMENT))
					world.obj.add(new TrafLight(x + World.PARTSIZE, y + World.PARTSIZE, z + 2, 180));
				x = pos.x + dim.x;
				y = pos.y + dim.y / 2;
				if(!World.isBlock(x,y,z) && (World.isBlock(x, y, z -1 ) && Resources.getBlock((int) x, (int) y, (int) z - 1).ground_type != BlockInfo.PAVEMENT))
					world.obj.add(new TrafLight(x + World.PARTSIZE + 1, y + World.PARTSIZE, z + 2, 0));
			}
		}
	}

	public void update(float delta) {
		trafficCount += delta;

		if(trafficCount > 5) {
			//trafficDirection = 2;
			if(trafficCount > 7) {
				if(trafficDirection == 0)
					trafficDirection = 1;
				else trafficDirection = 0;

				trafficCount = 0;
			}

		}

		List<Zone> zones = Resources.zones;
		for(int i = 0; i < zones.size(); i++) {
			Zone zone = zones.get(i);
			if(zone.getType() == 2) {
				Vector2 pos = zone.getPosition();
				Vector2 dim = zone.getDimensions();
				int x = (int) pos.x;
				int y = (int) pos.y;

				if(trafficDirection == 0) {
					for(int j = 0; j < dim.y; j++)
						if(World.getRoad(x - 1 + World.PARTSIZE, y + j + World.PARTSIZE) != -1)  World.road_blocked[x - 1][y + j] = true;
					for(int j = 0; j < dim.y; j++)
						if(World.getRoad(x + dim.x + World.PARTSIZE, y + j + World.PARTSIZE) != -1)  World.road_blocked[x + (int) dim.x][y + j] = true;
				}

				if(trafficDirection == 1) {
					for(int j = 0; j < dim.x; j++)
						if(World.getRoad(x + j + World.PARTSIZE, y - 1 + World.PARTSIZE) != -1) World.road_blocked[x + j][y - 1] = true;
					for(int j = 0; j < dim.x; j++)
						if(World.getRoad(x + j + World.PARTSIZE, y + dim.y + World.PARTSIZE) != -1)  World.road_blocked[x + j][y + (int) dim.y] = true;
				}
			}
		}
	}

	public boolean isZone(float x, float y) {
		return traffic_zone[(int)x][(int)y];
	}

	public boolean isVisible(float x, float y) {
		return !(y > Renderer.viewPort[3] || y < Renderer.viewPort[1] || x < Renderer.viewPort[0] || x > Renderer.viewPort[2]);
	}
}
