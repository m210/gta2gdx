//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx;

import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.map.MapPart.MeshType;
import ru.m210projects.gta2gdx.objects.SDebug;
import ru.m210projects.gta2gdx.objects.Sprite;
import ru.m210projects.gta2gdx.style.FontBase;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class Renderer {
	PerspectiveCamera cam;
	FirstPersonCameraController fpc;
	SpriteBatch batch;

	World world;
	ShaderProgram shader;
	ShaderProgram spr_shader;
	ShaderProgram hud_shader;
	private float aspect;
	public static double[] viewPort = new double[6];
	SDebug deb;
	public Renderer(World world) {

		deb = new SDebug(world.player.pos.x, world.player.pos.z, world.player.pos.y);


		this.world = world;
		batch = new SpriteBatch();

		cam = new PerspectiveCamera(45, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(world.player.pos.x, 9.12f, world.player.pos.z);
        cam.lookAt(cam.position.x, 0, cam.position.z);

        //fpc = new FirstPersonCameraController(cam);
        //fpc.setVelocity(30);

        aspect = cam.viewportWidth / cam.viewportHeight;
        Gdx.input.setInputProcessor(fpc);

        String vertexShader = Gdx.files.internal("shaders/world_vertex.glsl").readString();
        String fragmentShader = Gdx.files.internal("shaders/world_fragment.glsl").readString();

        shader = new ShaderProgram(vertexShader, fragmentShader);
        if(!shader.isCompiled())
        	System.err.println("world_shader " + spr_shader.getLog());

        String sprite_vertexShader = Gdx.files.internal("shaders/sprites_vertex.glsl").readString();
        String sprite_fragmentShader = Gdx.files.internal("shaders/sprites_fragment.glsl").readString();

        spr_shader = new ShaderProgram(sprite_vertexShader, sprite_fragmentShader);
        if(!spr_shader.isCompiled())
        	System.err.println("spr_shader " + spr_shader.getLog());

        String hud_vertexShader = Gdx.files.internal("shaders/hud_vertex.glsl").readString();
        String hud_fragmentShader = Gdx.files.internal("shaders/hud_fragment.glsl").readString();

        hud_shader = new ShaderProgram(hud_vertexShader, hud_fragmentShader);
        if(!hud_shader.isCompiled())
        	System.err.println("hud_shader " + spr_shader.getLog());

        Gdx.gl.glCullFace(GL20.GL_BACK);
	}

	boolean on = true;

	public void render() {
		cam.update();
		world.update(Gdx.graphics.getDeltaTime());

		if(Gdx.input.isKeyPressed(Keys.PLUS)) {
        	cam.position.y -= 10 * Gdx.graphics.getDeltaTime();
		}

        if(Gdx.input.isKeyPressed(Keys.MINUS)) {
        	cam.position.y += 10 * Gdx.graphics.getDeltaTime();
        }

        //if(cam.position.y > 15) cam.position.y = 15;
        //if(cam.position.y < 1) cam.position.y = 1;
		cam.position.x = world.player.pos.x;
		cam.position.z = world.player.pos.z;

		viewPort[4] = getFrustumHeight(cam.position.y); //viewHeight
		viewPort[5] = getFrustumWidth(viewPort[4], aspect); //viewWidth

		viewPort[0] = cam.position.x - viewPort[5] / 2; //left coordX
		viewPort[1] = cam.position.z - viewPort[4] / 2; // left coordY
		viewPort[2] = viewPort[0] + viewPort[5]; //right coordX
		viewPort[3] = viewPort[1] + viewPort[4]; //right coordY

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        Gdx.gl.glEnable(GL20.GL_CULL_FACE);
		Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glEnable(GL20.GL_TEXTURE_2D);
			if(Gdx.input.isKeyJustPressed(Keys.O))
				on = !on;
		if(on)
			draw_world();



		draw_sprites();

		Gdx.gl.glDisable(GL20.GL_CULL_FACE);
		Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glDisable(GL20.GL_TEXTURE_2D);

		draw_hud();

		for(int x = 0; x < 256; x++)
			for(int y = 0; y < 256; y++)
				World.road_blocked[x][y] = false;





	}

	int spriteid = 893;
	private void draw_hud() {
		FontBase font = Resources.fontbases[1];

		batch.begin();

		Resources.palettes[font.palnum].getTexture().bind(1);
		hud_shader.setUniformi("u_pal", 1);
		Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
		batch.setShader(hud_shader);

		font.draw(batch, Integer.toString((int) (Runtime.getRuntime().totalMemory() / (1024 * 1024))) + " mb", 150, Gdx.graphics.getHeight() - 30);
	    font.draw(batch, Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024))) + " mb /", 50, Gdx.graphics.getHeight() - 30);

	    font.draw(batch, Integer.toString(Main.totalTime) + " ms", 50, Gdx.graphics.getHeight() - 60);
	    font.draw(batch, Integer.toString(Gdx.graphics.getFramesPerSecond()) + " fps", 50, Gdx.graphics.getHeight() - 90);



	    batch.end();

//	    if(Gdx.input.isKeyJustPressed(Keys.Q)) {
//	    	spriteid--;
//	    	System.out.println(spriteid);
//	    }
//	    if(Gdx.input.isKeyJustPressed(Keys.E)) {
//	    	spriteid++;
//	    	System.out.println(spriteid);
//	    }
//		int palnum = Resources.paletteIndex[PaletteBase.SPRITE + spriteid];
//
//		Gdx.gl.glEnable(GL20.GL_BLEND);
//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
//
//
//		batch.begin();
//		Resources.palettes[palnum].getTexture().bind(1);
//		hud_shader.setUniformi("u_pal", 1);
//		Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
//		batch.setShader(hud_shader);
//		Texture tex = Sprite.getTexture(spriteid);
//		batch.draw(tex, 50, 50, 256,256);
//		batch.end();
//
//		Gdx.gl.glDisable(GL20.GL_BLEND);
	}

	private void draw_world() {
		int i;
		Resources.atlas.bind(0);
		shader.begin();
		shader.setUniformMatrix("u_projTrans", cam.combined);
		shader.setUniformi("u_alpha", 0);
		for(i = 0; i < Resources.getParts().length; i++) {
			MapPart part = Resources.getParts()[i];
			if(part.isVisible()) {
				Mesh mesh = part.getMesh(MeshType.Solid);
				if(mesh != null) mesh.render(shader, GL20.GL_TRIANGLES);
			}
		}

		Gdx.gl.glDepthFunc(GL20.GL_LEQUAL);
		shader.setUniformi("u_alpha", 1);
		for(i = 0; i < Resources.getParts().length; i++) {
			MapPart part = Resources.getParts()[i];
			if(part.isVisible()) {
				Mesh mesh = part.getMesh(MeshType.Alpha);
				if(mesh != null) mesh.render(shader, GL20.GL_TRIANGLES);
			}
		}
		shader.end();
	}

	private void draw_sprites() {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		spr_shader.begin();
		spr_shader.setUniformMatrix("u_projTrans", cam.combined);





//			deb.pos.x = b.getPosition().x;
//			deb.pos.z = b.getPosition().y;
//			deb.ang = b.getAngle();
//			deb.render(spr_shader);





		for(int i = 0; i < world.obj.size(); i++) {
			Sprite spr = world.obj.get(i);
			if(spr != null && spr.isVisible()) {
				spr.render(spr_shader);
			}
		}





		spr_shader.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
		Gdx.gl.glDepthFunc(GL20.GL_LESS);
	}

	public double getFrustumWidth(double frustumHeight, float aspect) {
		return frustumHeight * aspect;
	}

	public double getFrustumHeight(float distance) {
		return 2.0 * distance * Math.tan(Math.toRadians(cam.fieldOfView * 0.5));
	}

}
