//This file is part of Gta2GDX
//Copyright (C) 2016  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
//Gta2GDX is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.gta2gdx;

import ru.m210projects.gta2gdx.map.MapPart;
import ru.m210projects.gta2gdx.tools.MemLog;
import ru.m210projects.gta2gdx.tools.Timer;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;


public class Main extends ApplicationAdapter {

	World world;
	Renderer render;
	Resources res;
	static int totalTime = 0;
	String mapname = "wil";

	@Override
	public void create () {
		Timer.start();
		res = new Resources(mapname, mapname);

		for(int i = 0; i < Resources.getParts().length; i++)
			loadPart(i);
		Timer.result("Resources");
		world = new World();
		render = new Renderer(world);

		totalTime = (int) Timer.result("End");
//		Gdx.input.setCursorCatched(true);
		MemLog.log("End");
	}

	public void loadPart(int num) {
		int a = num / (World.MAPPARTS);
		int b = num % (World.MAPPARTS);

		MapPart[] parts = Resources.getParts();

		if(a == 0) {
			parts[num].initEdge(b, MapPart.LEFT);
		} else if(b == 0 && a != 0 && a != 17) {
			parts[num].initEdge(a, MapPart.TOP);
		} else if(a == 17) {
			parts[num].initEdge(b, MapPart.RIGHT);
		} else if(b == 17 && a != 0 && a != 17) {
			parts[num].initEdge(a, MapPart.BOTTOM);
		} else {
			int x = ((num - (World.MAPPARTS + 1)) / (World.MAPPARTS)) * World.PARTSIZE;
			int y = ((num - (World.MAPPARTS + 1)) % (World.MAPPARTS)) * World.PARTSIZE;
			parts[num].init(new Vector2(x, y));
		}
		if(Config.lights)
			parts[num].calc_staticLights();
		parts[num].buildMesh();
	}

	@Override
	public void render () {
		render.render();
	}
}
