package ru.m210projects.gta2gdx.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import ru.m210projects.gta2gdx.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.backgroundFPS =0;
		config.foregroundFPS =0;
		config.vSyncEnabled = false;
//		config.fullscreen = true;
		//config.width = 1024;
		//config.height = 768;
		new LwjglApplication(new Main(), config);
	}
}
